﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Home_pro_detail2 : System.Web.UI.Page
{
    protected DataTable dt;
    protected string name;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["id"] != null)
        {
            name = MDB.ReadScalar("kgm_product_classification", "title", "id=" + Request["id"]).ToString();

            dt = MDB.ReadTable("kgm_product", "*", "classification='" + name + "'");
        }
    }

}