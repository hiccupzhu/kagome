﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="Home_index" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>可果美</title>
<link href="css/reset.css" rel="stylesheet" type="text/css">
<link href="css/index.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="index_bd"> 
	<!-- 无活动banner -->          
	<div class="banner">
    	
        <p class="car_banner" title="头部小车"></p>
        <ul id="adBig" class="ad">
        	<li class="ban1" style=<%= "background:url(" + row["imgUrl"] + ") center 0 no-repeat;" %> istrue="true">
            	<div class="li-content">
                    <h1 id="logo" class="logo"></h1> 
                    <a href=<%= row["hyperlink"] %>><p id="banner_1" class="pic_txt"><%= row["title"] %></p></a>
                    <img class="banner_pro" src="img/banner_pro.png">
                    <div class="t_big" title="番茄"></div>
                    <img src="img/banner_pro_small.png" class="banner_pro_tl" />
                </div>
        	</li>
        	<!-- <li class="ban2" istrue="false" style="background-image:url(img/indexBanner/banner2/banner0.jpg);">
            	<div class="li-content">
                    <div class="J_b2_1" style="left:595px;top:495px;position:absolute; z-index:4;"><img src="img/indexBanner/banner2/fqb.png" /></div>
                    <div class="J_b2_2" style="left:580px;top:645px;position:absolute; z-index:2;"><img src="img/indexBanner/banner2/fqb_ty.png" /></div>
                </div>
            </li> -->
        </ul>        
    </div>
	<!-- 有活动banner -->          
	<div class="banner_active dis_n">
    	<div class="t_small" title="番茄"></div>
    	<div class="active_box">
        	<img class="active_pro" src="img/active_pro.png">
            

			<div class="active_bg"></div>
            <div id="slide_bd">
                <ul class="slide_list">
                    <li><a href="#"><img src="img/active_img1.jpg"></a></li>
                    <li><a href="#"><img src="img/active_img1.jpg"></a></li>
                    <li><a href="#"><img src="img/active_img1.jpg"></a></li>
                    <li><a href="#"><img src="img/active_img1.jpg"></a></li>
                </ul>
            </div>            
        </div>

	</div>

    
	<div class="way_bg">

    	<div class="content">
        	<!--导航-->
        	<div class="nav_box">
            	<ul class="nav">
                	<li class="nth_1 cur"><a href="index.aspx">首页</a><span class="arr_link"></span></li>
                    <li><a href="story_txt.aspx">品牌介绍</a><span class="arr_link"></span></li>
                    <li><a href="product.aspx">商品信息</a><span class="arr_link"></span></li>
                    <li><a href="active_list.aspx">宣传活动</a><span class="arr_link"></span></li>
                    <li class="nth_last"><a href="shanghai.aspx">可果美上海</a><span class="arr_link"></span></li>
                </ul>
            	<div class="weiboxin"><a class="weixin" href="#"></a><a class="weibo" href="#"></a></div>
            </div>

            <a id="J_car_temp" style="position:absolute; z-index:5; top:83px; left:70px; width:85px; height:85px;display:block;">
            	<img class="J_pf_z" src="img/czz.png" style="position:absolute;top:0;left:0;" />
            	<img class="J_pf_f" src="img/czf.png" style=" display:none;position:absolute;top:0;left:0;" />
            </a>

            <!--品牌故事-->
            <div class="story_box">
                <div class="road-1"></div>
            	<h2 class="lm_tit" title="可果美品牌故事"><em class="dis_n">可果美品牌故事</em></h2>
                <div class="story_txt" id="lm_1_1">
                	<h3 class="tit_h3" title="素质生活"></h3>
                	<p class="p">可果美以100%不添加的天然优质蔬菜汁/蔬果汁为身体补充每日素食需求。</p>
                	<h3 class="tit_h3 meixue" title="饮养美学"></h3>
                	<p class="p">通过最理想食材配比，创造出最佳蔬果饮养方案，以一份坚持呈现“蔬果之美”的匠心精神。</p>
                    <a class="more_link" href="story_txt.aspx" title="更多">
                        <!-- <img src="img/lm_2c_4.png"> -->
                    </a>
                </div>                
                <!--<img id="lm_1_2" class="story_img" src="img/lm_1.png" />-->
                <img id="lm_1_2" class="story_img1" src="img/lm_1img1.png">
                <img id="lm_1_img2" class="story_img2" src="img/lm_1img2.png">
                <img id="lm_1_img3" class="story_img3" src="img/lm_1img3.png">
                
                <p class="ico_house"></p>
            </div>
            <!--产品介绍-->
            <div class="product_box">
                 <div class="road-2"></div>
            	<h2 class="lm_tit" title="产品介绍"><em class="dis_n">产品介绍</em></h2>
                <div class="drink_show clearfix">
                	
                	<div class="J_show" >
                        <div class="drink_img tomato"><img src="img/home/h_07.png"></div>
                        <div class="txt_r">
                            <h3 class="tit_h3 tomato"></h3>
                            <p class="p">
    							保留番茄的天然美味和营养，富含茄红素， 一瓶280ml≈4只完熟番茄，蕴含美丽之秘的太阳果实，焕发你的天然美。百年经典，给你100%不添加的健康守护！</p>
                            <a class="more_link" href="product.aspx" title="更多">
                                <!-- <img src="img/lm_2c_4.png"> -->
                            </a>
                        </div>
                    </div>
                    
                    <div class="J_show" style="display:none;">
						<div class="drink_img" ><img src="img/home/h_02.png">
						</div>
						<div class="txt_r">
							<h3 class="tit_h3"></h3>
							<p class="p">
								当水果的清新口感遇见蔬菜的健康营养，让缤纷的蔬果色，开启每天的多彩生活；<br />
								果蔬生活为你轻松补充每天所需的多种蔬果营养，让你均衡每一天，健康活力每一天；
								</p>
							<a class="more_link" href="product.aspx" title="更多"> <!-- <img src="img/lm_2c_4.png"> --> </a>
						</div>
					</div>
                	
                </div>
                <div class="drink_list">
                    <div class="road-3"></div>
                	<ul class="list">
                    	<li class="li on" id="d_img1">
                            <p class="tip_bg">百年经典，给你100%<br>不添加的健康守护！</p>
                            <img class="pic" src="img/home/h_04.png">
                            <p class="a_link"><span class="btnico_bg"></span>纯正系列</p>
                        </li>
                    	<li class="li" id="d_img2">
                            <p style="padding-top: 18px;" class="tip_bg green">果蔬生活为你<br>轻松补充每天所需的<br>多种蔬果营养</p>
                            <img class="pic" src="img/home/h_05.png">
                            <p class="a_link"><span class="btnico_bg loubu_bg"></span>果蔬生活系列</p>
                        </li>

                    
                    	<!--
                    	<li class="on" id="d_img1">
                            <p class="tip_bg">文本文本11<br>喝出睛彩每一天</p>
                            <p class="img img1"></p>
                            <p class="a_link"><span class="btnico_bg"></span>蔬菜系列</p>
                        </li>
                    	<li id="d_img2">
                            <p class="tip_bg">文本文本22<br>喝出睛彩每一天</p>
                            <p class="img img2"></p>
                            <p class="a_link"><span class="btnico_bg loubu_bg"></span>蔬菜系列</p>
                        </li>
                    	<li id="d_img3">
                            <p class="tip_bg">文本文本33<br>喝出睛彩每一天</p>
                            <p class="img img3"></p>
                            <p class="a_link"><span class="btnico_bg jvzi_bg"></span>蔬菜系列</p>
                        </li>
                        -->
                    </ul>
                </div>
            	<p class="ico_house"></p>
            </div>
            <!--蔬菜价值-->
            <div class="value_box">
            	<h2 class="lm_tit" title="蔬菜价值"><em class="dis_n">蔬菜价值</em></h2>
                <img id="s4car" class="value_img1" src="img/lm_3c_img.png">
                <div class="value_img2">
                	<img class="lm_3c_i1" id="lm_3c_i1" src="img/lm_3c_i1.png">
                	<img class="lm_3c_i2" id="lm_3c_i2" src="img/lm_3c_i2.png">
                	<img class="lm_3c_i3" id="lm_3c_i3" src="img/lm_3c_i3.png">
                </div>
                <div class="value_right">
                	<h2 class="h3_lm3"><b>蔬菜汁的价值</b></h2>
                    <p class="txt_lm3">蔬菜汁是一种巧妙的蔬菜摄取方式之一，有了这一妙法，大家便可以轻松摄取蔬菜的美味和营养。<br>就让我们来重新认识蔬菜汁的独特价值！</p>
                    <a class="more_link" href="active_list.aspx" title="更多">
                        <!-- <img src="img/lm_2c_4.png"> -->
                    </a>
                    <div class="zhuanjia">
                    	<p class="say"><span class="user_ico"><img src="img/lm_3c_img3.png"></span><a href="#">来听听专家的在说些什么...</a></p>
                    </div>
                    
                </div>
                <p class="ico_house"></p>
            </div>
            <!--找到可果美-->
            <div class="find_box">
                <div class="road-4"></div>
                <div class="road-5"></div>
            	<h2 class="lm_tit" title="蔬菜价值"><em class="dis_n">蔬菜价值</em></h2>
                <div class="txt_lm4">
					<h3 class="h3_lm4"><b>企业概要和方针</b></h3>
                    <p class="p_lm4" style="word-break: break-all;width: 380px;">
                    	可果美（上海）饮料有限公司成立于2014年5月，公司以"True to nature, the flavor of KAGOME"为企业理念 ，以为消费者提供健康、营养、天然、美味的100%的蔬菜汁和蔬果汁为使命。通过饮食生活，可果美期许为人类、社会、地球环境的健康长寿而努力奉献。
                    	</p>
                    <p class="more_right">
                    	<a class="more_link" href="shanghai.aspx" title="更多">
                            <!-- <img src="img/lm_2c_4.png"> -->
                        </a>
                    </p>
                </div>
                <div class="map">
                	<h3 class="h3_lm4"><b>联系方式</b></h3>
                    <p class="p_lm4"><span class="tit_s">公司地址</span><span>上海市闵行区吴中路1688号A栋5F</span></p>
                    <p class="p_lm4"><span class="tit_s">电话咨询</span><span>021-52831266</span></p>
                    <p class="p_lm4"><span class="tit_s">热线服务时间</span><span>工作日 8:30～17:30</span></p>
                     <p class="p_lm4"><span class="tit_s">上海营业所地址</span><span>上海市徐汇区漕宝路70号C座26层2605室</span></p>
                    <img src="img/home/map.png">
                </div>
				
				<img class="loubu" src="img/lb.png">
				<p class="ico_house_small"></p>
				<p class="ico_house"></p>

				<!-- <img id="pro_img" class="pro_img" src="img/lm_4c_pro.png"> -->

            </div>            
        </div>
    </div>
    
    <div class="footer hei99">
    	<div class="content">        	
            <p class="foot_txt">Copyright 2012 KAGOME(SHANGHAI) FOODS CO.,Ltd.All Rights Reserved.    沪ICP备06025276号</p>
        </div>        
    </div>

</div>



<script src="js/jquery-1.8.3.min.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/jarallax-0.2.3b.js"></script>
<script src="js/TweenMax.min.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function (e) {
        //读取Banner条目数：0表示没有
        console.info(<%= bannerCount %>);

        //第一屏动画
        $(".nav li:not(.cur)").bind("mouseenter", function () {
            $(this).addClass("cur");
        }).bind("mouseleave", function () {
            $(this).removeClass("cur");
        });


        var animation_lm1 = new TimelineMax({
            onStart: function () {
            }, paused: true, onComplete: function () {

            }
        });
        animation_lm1.from('.story_box .ico_house', 0.5, { autoAlpha: 0, 'top': '+=50' })
				  .from('#lm_1_1', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.2')
				  .from('.story_box .lm_tit', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.2')
				  .from('#lm_1_2', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.4')
				  .from('#lm_1_img2', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.4')
				  .from('#lm_1_img3', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.4')




        var animation_lm2 = new TimelineMax({
            onStart: function () {
            }, paused: true, onComplete: function () {

            }
        });
        animation_lm2.from('.product_box .ico_house', 0.5, { autoAlpha: 0, 'top': '+=50' })
				  .from('.product_box .lm_tit', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.2')
				  .from('#d_img1', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.2')
				  .from('#d_img2', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.4')
				  .from('#d_img3', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.4')
				  .from('.drink_show', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.4')


        var animation_lm3 = new TimelineMax({
            onStart: function () {
            }, paused: true, onComplete: function () {

            }
        });

        animation_lm3.from('.value_box .ico_house', 0.5, { autoAlpha: 0, 'top': '+=50' })
				  .from('.value_box .lm_tit', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.2')
				  .from('#lm_3c_i1', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.2')
				  .from('#lm_3c_i2', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.4')
				  .from('#lm_3c_i3', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.4')
				  .from('#s4car', 0.5, { autoAlpha: 0, 'top': '+=80' }, '-=0.4')
				  .from('.value_right', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.4')


        var animation_lm4 = new TimelineMax({
            onStart: function () {

            }, paused: true, onComplete: function () {

            }
        });
        animation_lm4.from('.find_box .ico_house', 0.5, { autoAlpha: 0, 'top': '+=50' })
				  .from('.find_box .lm_tit', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.2')
				  .from('.find_box .loubu', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.2')
				  .from('.find_box .map', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.4')
				  .from('.find_box .txt_lm4', 0.5, { autoAlpha: 0, 'top': '+=50' }, '-=0.4')
				  .from('.find_box .ico_house_small', 0.5, { autoAlpha: 0, 'top': '+=80' }, '-=0.4')
        //.from('.find_box .pro_img',0.5,{autoAlpha:0,'top':'+=50'},'-=0.4')

        var _hasPlay = {
            d1: false,
            d2: false,
            d3: false,
            d4: false
        }



        var demo_lm1 = new TimelineMax({
            onStart: function () {

            }, paused: true, onComplete: function () {
                //animation_lm1.play();
            }
        });



        demo_lm1.to('#J_car_temp', 10, { bezier: [{ top: 83, left: 70 }, { top: 85, left: 210 }, { top: 65, left: 640 }, { top: 80, left: 1000 }, { top: 115, left: 1120 }], ease: 'Linear.easeNone' })
		// .to('#J_car_temp',0.8,{rotation:1},0.2)
		// .to('#J_car_temp',0.5,{rotation:-8},1.5)
		// .to('#J_car_temp',0.5,{rotation:0},2)
		// .to('#J_car_temp',0.5,{rotation:0},2.5)
		.to('#J_car_temp', 0.5, { rotation: 0, autoAlpha: 0, onComplete: function () { TweenMax.set('#J_car_temp', { top: 830 }); $('#J_car_temp .J_pf_f').fadeIn().siblings().fadeOut(); } }, 10)

		.to('#J_car_temp', 0.5, { autoAlpha: 1, left: 1080 }, 10.5)
		.to('#J_car_temp', 10, { bezier: [{ top: 830, left: 1080 }, { top: 855, left: 500 }, { top: 845, left: 40 }, { top: 840, left: -80 }], ease: 'Linear.easeNone' })
		.to('#J_car_temp', 0.5, { left: -100, autoAlpha: 0, onComplete: function () { TweenMax.set('#J_car_temp', { top: 1530 }); $('#J_car_temp .J_pf_z').fadeIn().siblings().fadeOut(); } }, 21)

		.to('#J_car_temp', 0.5, { left: -80, autoAlpha: 1 }, 21.5)
		.to('#J_car_temp', 10, { bezier: [{ top: 1530, left: 650 }, { top: 1540, left: 1100 }], ease: 'Linear.easeNone' })
		.to('#J_car_temp', 0.5, { left: 1170, autoAlpha: 0, onComplete: function () { TweenMax.set('#J_car_temp', { top: 2250 }); $('#J_car_temp .J_pf_f').fadeIn().siblings().fadeOut(); } }, 32)

		.to('#J_car_temp', 0.5, { left: 1170, autoAlpha: 1 }, 32.5)
		.to('#J_car_temp', 10, { bezier: [{ top: 2250, left: 1150 }, { top: 2260, left: 240 }, { top: 2330, left: -40 }], ease: 'Linear.easeNone' })
		.to('#J_car_temp', 0.5, { left: -60, autoAlpha: 0, onComplete: function () { TweenMax.set('#J_car_temp', { top: 3425 }); $('#J_car_temp .J_pf_z').fadeIn().siblings().fadeOut(); } }, 43)

		.to('#J_car_temp', 0.5, { left: -40, autoAlpha: 1 }, 43.5)
		.to('#J_car_temp', 2, { bezier: [{ top: 3425, left: 90 }, { top: 3430, left: 225 }], ease: 'Linear.easeNone' });




        var _oldTweenTime = 0, _newTweenTime = 0, _carScale = 1, _carTotalTime = demo_lm1._totalDuration, _pageTotalScroll = $('body').height() - $(window).height() * 1.5;

        $(window).scroll(function () {
            //console.log("T:"+$(document).scrollTop());
            //console.log($(window).height()/2);
            var win_h = parseInt($(window).height());
            if ($(document).scrollTop() > 1000 - win_h) {//品牌故事
                animation_lm1.play();
            }
            if ($(document).scrollTop() > 1800 - win_h) {//产品介绍动画
                animation_lm2.play();
            }

            if ($(document).scrollTop() > 1800) {//价值动画
                animation_lm3.play();
            }

            if ($(document).scrollTop() > 2500) {
                animation_lm4.play();
            }

            _newTweenTime = _carTotalTime * ($(window).scrollTop() / _pageTotalScroll);
            if (_newTweenTime > _oldTweenTime) {
                if (_oldTweenTime - _newTweenTime > 0.1) {
                    _carScale = (_oldTweenTime - _newTweenTime) / 0.1;
                } else {
                    _carScale = 1;
                }
                demo_lm1.timeScale(_carScale).tweenTo(_newTweenTime);
                _oldTweenTime = _newTweenTime;
            }

        });



        TweenMax.set('.index_bd .t_big', { left: '-=220', rotation: '-=45' });
        TweenMax.set('.J_b2_1', { left: '-=100', rotation: '-=35' });
        TweenMax.set('.J_b2_2', { left: '-=100' });

        var bannerAni_1 = new TimelineMax({
            onStart: function () {

            }, paused: true, onComplete: function () {
                //animation_lm1.play();
            }
        });
        bannerAni_1.to('.index_bd .t_big', 1, { left: '+=200', rotation: '+=45' })
        bannerAni_1.restart();



        var bannerAni_2 = new TimelineMax({
            onStart: function () {

            }, paused: true, onComplete: function () {
                //animation_lm1.play();
            }
        });
        bannerAni_2.to('.J_b2_1', 1, { left: '+=100', rotation: '+=35' })
        bannerAni_2.to('.J_b2_2', 1, { left: '+=100' }, '-=1')
        bannerAni_2.restart();

        // $(".drink_list li").on("click",function(){
        // var _ind = $(this).index();
        // $(".drink_show .J_show").fadeOut(600).eq(_ind).fadeIn(600);
        // });

        $(".drink_list li").hover(function () {
            $(this).siblings().removeClass("on");
            $(this).addClass("on");
            var _ind = $(this).index();
            $(".drink_show .J_show").hide().eq(_ind).show();
        }, function () {

        });

        if (window.location.href.split("?a=")[1] == 1) {
            $(".banner").addClass("dis_n");
            $(".banner_active").removeClass("dis_n");
            slideActive();
        } else {
            $(".banner").removeClass("dis_n");
            $(".banner_active").addClass("dis_n");

        }



        function slideActive() {
            var sWidth = $("#slide_bd").width(); //获取焦点图的宽度（显示面积）
            var len = $("#slide_bd ul li").length; //获取焦点图个数
            var index = 0;
            var picTimer;

            //以下代码添加数字按钮和按钮后的半透明条，还有上一页、下一页两个按钮
            var btn = "<div class='btn'>";
            for (var i = 0; i < len; i++) {
                btn += "<span></span>";
            }
            btn += "</div>";
            $("#slide_bd").append(btn);

            //为小按钮添加鼠标滑入事件，以显示相应的内容
            $("#slide_bd .btn span").css("opacity", 1).mouseenter(function () {
                index = $("#slide_bd .btn span").index(this);
                showPics(index);
            }).eq(0).trigger("mouseenter");

            //本例为左右滚动，即所有li元素都是在同一排向左浮动，所以这里需要计算出外围ul元素的宽度
            $("#slide_bd ul").css("width", sWidth * (len));
            //鼠标滑上焦点图时停止自动播放，滑出时开始自动播放
            $("#slide_bd").hover(function () {
                clearInterval(picTimer);
            }, function () {
                picTimer = setInterval(function () {
                    showPics(index);
                    index++;
                    if (index == len) { index = 0; }
                }, 4000); //此4000代表自动播放的间隔，单位：毫秒
            }).trigger("mouseleave");

            //显示图片函数，根据接收的index值显示相应的内容
            function showPics(index) { //普通切换
                var nowLeft = -index * sWidth; //根据index值计算ul元素的left值
                $("#slide_bd ul").stop(true, false).animate({ "left": nowLeft }, 300); //通过animate()调整ul元素滚动到计算出的position
                //$("#focus .btn span").removeClass("on").eq(index).addClass("on"); //为当前的按钮切换到选中的效果
                $("#slide_bd .btn span").stop(true, false).removeClass("on").eq(index).stop(true, false).addClass("on"); //为当前的按钮切换到选中的效果
            }

        }
    });

</script>
</body>
</html>

