﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="product.aspx.cs" Inherits="Home_product" %>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>可果美-商品信息</title>
		<link href="css/reset.css" rel="stylesheet" type="text/css">
		<link href="css/css.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.8.3.min.js"></script>
		<script src="js/com.js"></script>

	</head>
	<body>
		<div class="index_bd">
			<!--导航-->
			<div class="sub_header">
				<h1 class="logo"><em><img src="img/logo_fiexd.png" alt="可果美"></em></h1>
				<ul class="nav_ul">
					<li>
						<a href="index.aspx">首页</a><span class="arr_link"></span>
					</li>
					<li>
						<a href="story_txt.aspx">品牌介绍</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a">
								<a href="story_txt.aspx#pplj">品牌理念</a> | <a href="story_txt.aspx#zzgy">制作工艺</a> | <a href="story_txt.aspx#dsj">可果美大事记</a>
							</div>
						</div>
					</li>
					<li class="cur">
						<a href="product.aspx">商品信息</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-indent:200px;">
								<a href="product.aspx#pro_1">纯正系列</a> | <a href="product.aspx#pro_2">果蔬生活系列</a> | <a href="product.aspx#pro_3">果蔬汁的价值</a>
							</div>
						</div>

					</li>
					<li>
						<a href="active_list.aspx">宣传活动</a><span class="arr_link"></span>
					</li>
					<li>
						<a href="shanghai.aspx">可果美上海</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-align:right;">
								<a href="shanghai.aspx#qyfz">企业概要及方针</a> | <a href="shanghai.aspx#new_list">可果美新闻</a> | <a href="shanghai.aspx#qa">Q&amp;A</a> | <a href="shanghai.aspx#lxfs">联系方式</a> | <a href="shanghai.aspx#xsqd">销售渠道</a>
							</div>
						</div>
					</li>
				</ul>
				<p class="wei_bd">
					<a class="weixin" href="#" title="微信"></a><a class="weibo" href="#" title="微博"></a>
				</p>
			</div>

			<img class="pos_lpic" src="img/temp/l_03.png">
			<div class="content">
				<div class="wd1000 clearfix">

					<!--纯在系列-->
					<div class="chun_zheng">
						<div class="left_txt">
							<h2><a name="pro_1"></a><img src=<%= rows[0]["imgTitle"] %> /></h2>
							<h3><%= rows[0]["title"] %></h3>
							<p>
								<%= rows[0]["brief"] %>
							</p>
							<a class="a_link" href="pro_detail2.aspx?id=<%= rows[0]["id"] %>"><span class="btnico_bg"></span>查看详情</a>
						</div>
						<img class="r_pic1" src=<%= rows[0]["imgMain"] %>>
					</div>
				</div>
			</div>

			<!--果蔬生活系列-->
			<div class="guoshu_bg">
				<div class="wd1000 clearfix">
					<div class="guo_shu">
						<div class="left_txt">
							<h2><a name="pro_2"></a><img src=<%= rows[1]["imgTitle"] %> /></h2>
							<h3><%= rows[1]["title"] %></h3>
							<p>
								<%= rows[1]["brief"] %>
							</p>
							<p><img src="img/temp/txt18.png">
							</p>
							<a class="a_link" href="pro_detail2.aspx?id=<%= rows[1]["id"] %>"><span class="btnico_bg loubu_bg"></span>查看详情</a>
						</div>
						<img class="r_pic1" src=<%= rows[1]["imgMain"] %>>
					</div>
				</div>
			</div>

			<!--banner pic-->
			<a name="pro_3"></a>
			<div class="banner_pic"></div>

			<div class="content">
				<!--易吸收-->
				<div class="cptc_bd green_bd">
					<img class="tc_img" src="img/temp/yixi.png">
					<div class="green_txt">
						<h4><img src="img/temp/yixishou.png"></h4>
						<p>
							<em>营养吸收率上升</em>
							根据日本专业研究结果，比起生吃蔬菜，蔬菜榨成汁后，有些营养成分的吸收率会更高。
							<br />
							饮用蔬菜汁可以轻轻松松摄取多种蔬菜营养。
						</p>
						<p>
							<em>甚至是小朋友和老人，都可有效地吸收蔬菜汁中的营养。</em>
							若不用力咀嚼，吞咽，有的人会很难吃下蔬菜。蔬菜汁则老少皆宜，每个人都可以自然地保持健康的饮食习惯。
						</p>
					</div>
				</div>
				<!--营养-->
				<div class="cptc_bd red_bd">
					<img class="tc_img" src="img/temp/ying_img.png">
					<div class="red_txt">
						<h4><img src="img/temp/ying_tit.png"></h4>
						<p>
							<em>轻轻松松摄取多种蔬菜营养</em>
							营养均衡地摄取各种蔬菜非常重要，却很难坚持。
							<br />
							有了蔬菜汁便容易养成习惯，即使再忙碌，也可以轻轻松松每天饮用。
						</p>
						<p>
							<em>不必担心油，盐，糖等，蔬菜汁可单纯地提供蔬菜的营养。</em>
							如果想通过菜肴来摄取所需的蔬菜，则会不知不觉间摄取过多的油、盐味精等调料。蔬菜汁可帮助现代人摄取容易缺乏的蔬菜营养，而不必担心给身体带来负担。
						</p>
					</div>
				</div>
				<!--美味-->
				<div class="cptc_bd yellow_bd">
					<img class="tc_img" src="img/temp/1_19.png">
					<div class="yellow_txt">
						<h4><img src="img/temp/1_22.png"></h4>
						<p>
							<em>一年四季都可享受到时令蔬菜的美味。</em>
							时令是指蔬菜原本的盛产季节，此时蔬菜的口味和营养都处于顶峰时期。正是蔬菜汁最大程度地保留了时令蔬菜的口味和营养。
						</p>
						<p>
							<em>美味口感与生的蔬菜不同，不爱吃的蔬菜也可因此变的美味。</em>与水果搭配混合后，“营养很高，但味道不佳”的蔬菜也会变得惊人地美味！
						</p>
					</div>
				</div>

				<!--方便-->
				<div class="cptc_bd red_bd2">
					<img class="tc_img" src="img/temp/1_27.png">
					<div class="red2_txt">
						<h4><img src="img/temp/1_30.png"></h4>
						<p>
							<em>随时随地享用时令蔬菜</em>
							蔬菜汁浓缩了【时令蔬菜的营养和美味】，让你随时随地可以享用夏日采摘的茄红素含量最高的番茄美味、甚至享用从世界各地采购的蔬菜美味。
						</p>
						<p>
							<em>便于携带</em>
							即便是体积大又容易干掉的蔬菜也可浓缩成汁，非常便于携带，此外还可选择需要的量，随时随地开心享用。
						</p>
					</div>
				</div>

			</div>

			<div class="footer hei99">
				<div class="content">
					<p class="foot_txt">
						Copyright 2012 KAGOME(SHANGHAI) FOODS CO.,Ltd.All Rights Reserved.    沪ICP备06025276号
					</p>
				</div>
			</div>
		</div>
		<script type="text/javascript" charset="utf-8">
		    $(function () {
		        function setPos() {
		            var win_h = parseInt($(window).height());
		            if ($(document).scrollTop() >= 80) {
		                $(".pos_lpic").css({
		                    "top": "483px"
		                });
		            } else {
		                $(".pos_lpic").css({
		                    "top": "563px"
		                });
		            }
		        }

		        setPos();

		        $(window).scroll(function () {
		            setPos();
		        });
		    });
		</script>
	</body>
</html>

