﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class active_list : System.Web.UI.Page
{
    protected DataTable dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
             dt = GetDBData();
        }
    }


    public DataTable GetDBData()
    {
        string sql = "SELECT TOP 3 * FROM [kgm_activity] ORDER BY id DESC";

        DataTable dt = MDB.QueryTable(sql);

        return dt;
    }
}