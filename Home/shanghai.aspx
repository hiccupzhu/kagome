﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="shanghai.aspx.cs" Inherits="Home_shanghai" %>

<html>
	<head>
		<meta charset="utf-8">
		<title>可果美-可果美 上海</title>
		<link href="css/reset.css" rel="stylesheet" type="text/css">
		<link href="css/css.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.8.3.min.js"></script>
		<script src="js/com.js"></script>
	</head>
	<body>
		<div class="index_bd">
			<!--导航-->
			<div class="sub_header">
				<h1 class="logo"><em><img src="img/logo_fiexd.png" alt="可果美"></em></h1>
				<ul class="nav_ul">
					<li>
						<a href="index.aspx">首页</a><span class="arr_link"></span>
					</li>
					<li>
						<a href="story_txt.aspx">品牌介绍</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a">
								<a href="story_txt.aspx#pplj">品牌理念</a> | <a href="story_txt.aspx#zzgy">制作工艺</a> | <a href="story_txt.aspx#dsj">可果美大事记</a>
							</div>
						</div>
					</li>
					<li>
						<a href="product.aspx">商品信息</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-indent:200px;">
								<a href="product.aspx#pro_1">纯正系列</a> | <a href="product.aspx#pro_2">果蔬生活系列</a> | <a href="product.aspx#pro_3">果蔬汁的价值</a>
							</div>
						</div>

					</li>
					<li>
						<a href="active_list.aspx">宣传活动</a><span class="arr_link"></span>
					</li>
					<li class="cur">
						<a href="shanghai.aspx">可果美上海</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-align:right;">
								<a href="shanghai.aspx#qyfz">企业概要及方针</a> | <a href="shanghai.aspx#new_list">可果美新闻</a> | <a href="shanghai.aspx#qa">Q&amp;A</a> | <a href="shanghai.aspx#lxfs">联系方式</a> | <a href="shanghai.aspx#xsqd">销售渠道</a>
							</div>
						</div>
					</li>
				</ul>

				<p class="wei_bd">
					<a class="weixin" href="#" title="微信"></a><a class="weibo" href="#" title="微博"></a>
				</p>
			</div>

			<div class="content">
				<div class="wd1000 clearfix">
					<img class="pos_pic1" src="img/sh_img2.png">
					<img class="pos_pic2" src="img/sh_img22.png">
					<img class="pos_pic3" src="img/sh_img3.png">
					<!--企业概要及方针-->
					<div class="gaikuang_bd clearfix">
						<a name="qyfz"></a>
						<img class="gai_img" src="img/sh_img1.jpg">
						<div class="gai_txt">
							<h2 class="sh_h2">企业概要及方针</h2>
							<p>
								1933年，可果美推出了日本最早的蔬菜汁——可果美番茄汁，品牌成立至今，我们始终秉承着“田地是第一工厂”的理念，致力于栽培美味、安全的原料、生产高品质的蔬菜汁。
								<br>
								可果美进入中国市场伊始，就希望能够通过为消费者提供蔬菜汁的方式来传达蔬菜的重要性，鼓励大家“以饮料的形式来摄取蔬菜”，补充日常容易缺乏的蔬菜营养。可果美坚持“蔬菜汁是一种摄取蔬菜的方式”，它富含时令蔬菜的高营养价值和美味，让每个人可以随时摄取“时令蔬菜”，并且浓缩了蔬菜的精华，多种蔬菜均衡搭配，物超所值是可果美蔬菜汁的几大特点。虽然蔬菜有煮、炒、色拉等多种摄入方式，但对于忙碌的现代都市人来说却很难保证足够的量。那就把蔬菜汁作为一种“菜单”融入日常生活，点滴间增进自身健康吧。
							</p>
						</div>
					</div>
					<!--可果美新闻-->
                    <form  runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
					        <div class="news_bd">
                        
						        <a name="new_list"></a>
						        <h2 class="sh_h2"><span class="ico"></span>可果美新闻</h2>
						        <div class="list_news">
							        <ul>
                                
                                        <% for(int i = 0; i < dtn.Rows.Count; i++) { %>
                                        <li>
									        <span class="datetime"><%= ((DateTime)dtn.Rows[i][1]).ToString("yyyy/MM/dd") %></span>
									        <p class="p">
										        <span class="p_arr"></span><%= dtn.Rows[i][2].ToString() %>
									        </p>
                                            <a class="view_link" href="news_detail.aspx?id=<%= dtn.Rows[i][0].ToString() %>">查看详情</a>
								        </li>
                                        <%} %>
                                   
							        </ul>
						        </div>
						        <p class="page_bd">
                                    <asp:LinkButton CssClass="pre_a" OnClick="BtnFirstPage_Click" runat="server">上一页</asp:LinkButton>
                                    <asp:Label  ID="txtPageIndex" Text="0" Font-Underline="true" runat="server" />/
                                    <asp:Label  ID="txtPageCount" Text="0" Font-Underline="true" runat="server" />
                                    <asp:LinkButton CssClass="next_a" OnClick="BtnNextPage_Click" runat="server">下一页</asp:LinkButton>
                                    <!--
							        <a class="pre_a" href="#">上一页</a>
							        <a href="#">1</a><a href="#">2</a><a href="#">3</a> ... <a href="#">4</a><a href="#">5</a><a href="#">6</a>
							        <a class="next_a" href="#">下一页</a>
                                    -->
						        </p>

                          
					        </div>

					<!--Q&A-->
					        <div class="q_a_bd">
						        <a name="qa"></a><asp:Label ID="txtQA" Visible="false" runat="server"></asp:Label>
						        <h2 class="sh_h2"><span class="ico"></span>Q&amp;A</h2>
                                <% for(int i =0; i < dtq.Rows.Count; i ++){ %>
						        <div class="qa_list">
							        <p class="qu_p">
								        <span class="qu_ico"></span><b><%= dtq.Rows[i][1].ToString() %></b>
							        </p>
							        <p class="an_p">
								        <span class="an_ico"></span><%= dtq.Rows[i][2].ToString() %>
							        </p>
						        </div>
                                <%} %>
						
                                <!--
						        <a class="more_qa" href="#"></a>
                                -->
                                <asp:LinkButton CssClass="more_qa" ID="MoreQA" OnClick="MoreQA_Click" runat="server"></asp:LinkButton>

                            
					    </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                     </form>

					<!--联系方式-->
					<div class="lxfs_bd">
						<a name="lxfs"></a>
						<h2 class="sh_h2"><span class="ico"></span>联系方式</h2>
						<ul>
							<li>
								消费者热线：021-52831266
							</li>
							<li>
								热线服务时间：工作日 8:30～17:30
							</li>
							<li>
								公司办公地址：上海市闵行区吴中路1688号A栋5F
							</li>
							<!-- <li>
								上海营业所地址：上海市徐汇区漕宝路70号C座26层2605室
							</li> -->
						</ul>
					</div>
					<!--可果美中国销售渠道-->
					<div class="sale_bd">
						<a name="xsqd"></a>
						<h2 class="sh_h2"><span class="ico"></span>可果美中国销售渠道</h2>

						<img src="img/sh_map.jpg">
					</div>

				</div>
			</div>

			<div class="footer hei99">
				<div class="content">
					<p class="foot_txt">
						Copyright 2012 KAGOME(SHANGHAI) FOODS CO.,Ltd.All Rights Reserved.    沪ICP备06025276号
					</p>
				</div>
			</div>
		</div>

	</body>
</html>
