﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Home_product : System.Web.UI.Page
{
    
    protected DataRow[] rows = new DataRow[2];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt = MDB.ReadTable("kgm_product_classification", "*");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i > 1) break;
                rows[i] = dt.Rows[i];
            }
        }
    }
}