﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Home_story_txt : System.Web.UI.Page
{
    protected DataTable dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDataView();
        }
    }

    protected void BindDataView()
    {
        dt = GetBNewsData();
    }

    public DataTable GetBNewsData()
    {
        string sql = "SELECT TOP 7 * FROM [kgm_breaknews] ORDER BY id ASC";

        DataTable dt = MDB.QueryTable(sql);

        return dt;
    }
}