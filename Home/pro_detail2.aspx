﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pro_detail2.aspx.cs" Inherits="Home_pro_detail2" %>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>可果美-产品详情</title>
		<link href="css/reset.css" rel="stylesheet" type="text/css">
		<link href="css/css.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.8.3.min.js"></script>
		<script src="js/com.js"></script>

	</head>
	<body>
		<div class="index_bd">
			<!--导航-->
			<div class="sub_header">
				<h1 class="logo"><em><img src="img/logo_fiexd.png" alt="可果美"></em></h1>
				<ul class="nav_ul">
					<li>
						<a href="index.aspx">首页</a><span class="arr_link"></span>
					</li>
					<li>
						<a href="story_txt.aspx">品牌介绍</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a">
								<a href="story_txt.aspx#pplj">品牌理念</a> | <a href="story_txt.aspx#zzgy">制作工艺</a> | <a href="story_txt.aspx#dsj">可果美大事记</a>
							</div>
						</div>
					</li>
					<li class="cur">
						<a href="product.aspx">商品信息</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-indent:200px;">
								<a href="product.aspx#pro_1">纯正系列</a> | <a href="product.aspx#pro_2">果蔬生活系列</a> | <a href="product.aspx#pro_3">果蔬汁的价值</a>
							</div>
						</div>

					</li>
					<li>
						<a href="active_list.aspx">宣传活动</a><span class="arr_link"></span>
					</li>
					<li>
						<a href="shanghai.aspx">可果美上海</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-align:right;">
								<a href="shanghai.aspx#qyfz">企业概要及方针</a> | <a href="shanghai.aspx#new_list">可果美新闻</a> | <a href="shanghai.aspx#qa">Q&amp;A</a> | <a href="shanghai.aspx#lxfs">联系方式</a> | <a href="shanghai.aspx#xsqd">销售渠道</a>
							</div>
						</div>
					</li>
				</ul>

				<p class="wei_bd">
					<a class="weixin" href="qrcode.aspx" target="_blank" title="微信"></a><a class="weibo" href="#" title="微博"></a>
				</p>
			</div>

			<div class="content main-content">
				<div class="pro_detail">
					<div class="tag_bd">
                        <!--
						<p class="tag_p">
							<span class="ico pg"></span>纯正系列
						</p>
                        -->
						<p class="tag_p cur">
							<span class="ico loubu"></span><%= name %>
						</p>
					</div>
					<div class="tag_content">
						<div class="con_bd clearfix">
							<div class="pro_left">
								<div class="big_pic"><img id="J_mainPic" src=<%= dt.Rows[0]["imgUrl"] %>>
								</div>
								<div class="thubmnail_list J_tablist">
                                    <% for(int i = 0; i < dt.Rows.Count; i++) { %>
                                    <% if(i == 0){ %>
                            	    <div class="thumbnail cur">
                                    <% } else { %>
                                    <div class="thumbnail">
                                    <% } %>
									
										<div class="small_pic">
											<img class="J_showimg" src="<%= dt.Rows[i]["imgUrl"] %>" data-src="<%= dt.Rows[i]["imgUrl"] %>">
										</div>
										<div class="title">
											<%= dt.Rows[i]["name"]  %>
										</div>
									</div>
									<% } %>

								</div>
							</div>
							<div class="pro_right J_proright">
                                <% for(int i = 0; i < dt.Rows.Count; i++) { %>
                                    <% if(i == 0){ %>
                            	    <div class="J_roright_con">
                                    <% } else { %>
                                    <div class="J_roright_con" style="display:none;">
                                    <% } %>
									    <%= dt.Rows[i]["content"] %>
                                    </div>
                                <% } %>
                            	
							</div>
						</div>
						<div class="con_bd clearfix dis_n">
							<div class="pro_left"></div>
							<div class="pro_right"></div>
						</div>
					</div>
				</div>
			</div>

			<div class="footer hei99">
				<div class="content">
					<p class="foot_txt">
						Copyright 2012 KAGOME(SHANGHAI) FOODS CO.,Ltd.All Rights Reserved.    沪ICP备06025276号
					</p>
				</div>
			</div>
		</div>


	<script>
	$(function(){
			$('.J_tablist .thumbnail').bind('click',function(){
				var $this = $(this), _index = $this.index();
					$('.J_proright .J_roright_con').eq(_index).show().siblings().hide();
					$this.addClass('cur').siblings().removeClass('cur');
					$('#J_mainPic').attr('src',$this.find('.small_pic .J_showimg').attr('data-src'))
			});
	});
	</script>

	</body>
</html>
