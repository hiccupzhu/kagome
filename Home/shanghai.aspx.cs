﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Home_shanghai : System.Web.UI.Page
{
    protected int mPageSize = 4;
    protected int mPageIndex;
    protected int mPageCount;
    protected int mRecordCount;

    protected DataTable dtn;
    protected DataTable dtq;

    protected void Page_Load(object sender, EventArgs e)
    {
        mRecordCount = MDB.Count("kgm_news");
        mPageCount = (mRecordCount + (mPageSize - 1)) / mPageSize;
        mPageIndex = Math.Max(0, Convert.ToInt32(txtPageIndex.Text) - 1);

        if (txtQA.Text == "")
        {
            txtQA.Text = "4";
        }

        if (!IsPostBack)
        {
            BindDataView();
        }
    }

    protected void BindDataView()
    {
        dtn = GetNewsData();
        dtq = GetQAData();

        txtPageIndex.Text = (mPageIndex + 1).ToString();
        txtPageCount.Text = mPageCount.ToString();
    }

    public DataTable GetNewsData()
    {
        int per_size = MUtils.CalcPerpageSize(mPageIndex, mPageCount, mPageSize, mRecordCount);

        string sql =
        "SELECT * FROM (" +
            "SELECT TOP " + per_size + " * FROM ( " +
                "SELECT TOP " + (mPageSize * (mPageIndex + 1)) + " id,createddate,brief FROM [kgm_news] ORDER BY id DESC" +
            " ) ORDER BY id ASC" +
        ")ORDER BY id DESC";

        DataTable dt = MDB.QueryTable(sql);

        return dt;
    }


    public DataTable GetQAData()
    {
        int per_size = Convert.ToInt32(txtQA.Text);

        string sql =
        "SELECT * FROM (" +
            "SELECT TOP " + per_size + " * FROM ( " +
                "SELECT TOP " + (per_size) + " id,question,answer FROM [kgm_qa] ORDER BY id DESC" +
            " ) ORDER BY id ASC" +
        ")ORDER BY id DESC";

        DataTable dt = MDB.QueryTable(sql);

        return dt;
    }

    protected void BtnFirstPage_Click(object sender, EventArgs e)
    {
        mPageIndex = 0;
        BindDataView();
    }

    protected void BtnNextPage_Click(object sender, EventArgs e)
    {
        mPageIndex = Math.Min(mPageIndex + 1, mPageCount - 1);

        BindDataView();
    }

    protected void MoreQA_Click(object sender, EventArgs e)
    {
        txtQA.Text = (Convert.ToInt32(txtQA.Text) + 4).ToString();
        BindDataView();
    }
}