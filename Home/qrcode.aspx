﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="qrcode.aspx.cs" Inherits="Home_qrcode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/jquery.qrcode-0.11.0.min.js"></script>
    
    <script type="text/javascript">
        $(function () {
            console.info("document.referrer=" + document.referrer);

            $("#qrcode").qrcode({
                "width": 300,
                "height": 300,
                "color": "#3a3",
                "text": document.referrer
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="qrcode" style="width:100%; margin:0 auto; text-align:center;">
    
    </div>
    </form>
</body>
</html>
