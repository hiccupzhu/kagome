﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Home_index : System.Web.UI.Page
{
    protected DataRow row;
    protected int bannerCount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData2View();
        }

        bannerCount = MDB.Count("kgm_index_banner");
    }

    protected void BindData2View()
    {
        DataTable dt = MDB.ReadTable("kgm_index_banner", "*");
        if (dt.Rows.Count < 1) return;
        row = dt.Rows[0];
    }
}