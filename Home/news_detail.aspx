﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="news_detail.aspx.cs" Inherits="Home_news_detail" %>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>可果美-产品详情</title>
		<link href="css/reset.css" rel="stylesheet" type="text/css">
		<link href="css/css.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.8.3.min.js"></script>
		<script src="js/com.js"></script>

	</head>
	<body>
		<div class="index_bd">
			<!--导航-->
			<div class="sub_header">
				<h1 class="logo"><em><img src="img/logo_fiexd.png" alt="可果美"></em></h1>
				<ul class="nav_ul">
					<li>
						<a href="index.aspx">首页</a><span class="arr_link"></span>
					</li>
					<li>
						<a href="story_txt.aspx">品牌介绍</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a">
								<a href="story_txt.aspx#pplj">品牌理念</a> | <a href="story_txt.aspx#zzgy">制作工艺</a> | <a href="story_txt.aspx#dsj">可果美大事记</a>
							</div>
						</div>
					</li>
					<li>
						<a href="product.aspx">商品信息</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-indent:200px;">
								<a href="product.aspx#pro_1">纯正系列</a> | <a href="product.aspx#pro_2">果蔬生活系列</a> | <a href="product.aspx#pro_3">果蔬汁的价值</a>
							</div>
						</div>

					</li>
					<li class="cur">
						<a href="active_list.aspx">宣传活动</a><span class="arr_link"></span>
					</li>
					<li>
						<a href="shanghai.aspx">可果美上海</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-align:right;">
								<a href="shanghai.aspx#qyfz">企业概要及方针</a> | <a href="shanghai.aspx#new_list">可果美新闻</a> | <a href="shanghai.aspx#qa">Q&amp;A</a> | <a href="shanghai.aspx#lxfs">联系方式</a> | <a href="shanghai.aspx#xsqd">销售渠道</a>
							</div>
						</div>
					</li>
				</ul>

				<p class="wei_bd">
					<a class="weixin" href="#" title="微信"></a><a class="weibo" href="#" title="微博"></a>
				</p>
			</div>

			<div class="content">
				<div class="news_detail">
					<h1 class="h1_tit"><%= row["title"] %></h1>
					<p class="time">
						<%= row["createddate"] %>
					</p>
					<div class="share_bd">
						<span class="span">分享到：</span><a href="#"><img class="share_img" src="img/share_weixin.jpg"></a><a href="#"><img class="share_img" src="img/share_weibo.jpg"></a>
					</div>
					<p class="jianjie">
						<%= row["brief"] %>
					</p>
					<p class="news_p">
						<%= row["content"] %>
					</p>

				</div>
			</div>

			<div class="footer hei99">
				<div class="content">
					<p class="foot_txt">
						Copyright 2012 KAGOME(SHANGHAI) FOODS CO.,Ltd.All Rights Reserved.    沪ICP备06025276号
					</p>
				</div>
			</div>
		</div>

	</body>
</html>
