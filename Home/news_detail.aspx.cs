﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Home_news_detail : System.Web.UI.Page
{
    protected DataRow row;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["id"] != null)
            {
                DataTable dt = BindDataView(Request["id"]);
                row =dt.Rows[0];
            }
            
        }
    }

    public DataTable BindDataView(string id)
    {
        string sql = "SELECT * FROM [kgm_news] where id=" + id;

        DataTable dt = MDB.QueryTable(sql);

        return dt;
    }
}