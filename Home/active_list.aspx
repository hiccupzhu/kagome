﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="active_list.aspx.cs" Inherits="active_list" %>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>可果美-宣传活动</title>
		<link href="css/reset.css" rel="stylesheet" type="text/css">
		<link href="css/css.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.8.3.min.js"></script>
		<script src="js/com.js"></script>

	</head>
	<body>
		<div class="index_bd">
			<!--导航-->
			<div class="sub_header">
				<h1 class="logo"><em><img src="img/logo_fiexd.png" alt="可果美"></em></h1>
				<ul class="nav_ul">
					<li>
						<a href="index.aspx">首页</a><span class="arr_link"></span>
					</li>
					<li>
						<a href="story_txt.aspx">品牌介绍</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-indent:50px;">
								<a href="story_txt.aspx#pplj">品牌理念</a> | <a href="story_txt.aspx#zzgy">制作工艺</a> | <a href="story_txt.aspx#dsj">可果美大事记</a>
							</div>
						</div>
					</li>
					<li>
						<a href="product.aspx">商品信息</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-indent:200px;">
								<a href="product.aspx#pro_1">纯正系列</a> | <a href="product.aspx#pro_2">果蔬生活系列</a> | <a href="product.aspx#pro_3">果蔬汁的价值</a>
							</div>
						</div>

					</li>
					<li class="cur">
						<a href="active_list.aspx">宣传活动</a><span class="arr_link"></span>
					</li>
					<li>
						<a href="shanghai.aspx">可果美上海</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-align:right;">
								<a href="shanghai.aspx#qyfz">企业概要及方针</a> | <a href="shanghai.aspx#new_list">可果美新闻</a> | <a href="shanghai.aspx#qa">Q&amp;A</a> | <a href="shanghai.aspx#lxfs">联系方式</a> | <a href="shanghai.aspx#xsqd">销售渠道</a>
							</div>
						</div>
					</li>
				</ul>

				<p class="wei_bd">
					<a class="weixin" href="#" title="微信"></a><a class="weibo" href="#" title="微博"></a>
				</p>
			</div>

			<div class="active_bd">
				<div class="ac_slide" style="background:url(img/temp/11.jpg) 0 0 no-repeat;">
					<p class="left_pos"></p>
					<p class="right_pos"></p>
					<div class="preNext pre"></div>
					<div class="preNext next"></div>
					<div id="slide_bd">
						<ul class="slide_list">
                            <% for(int i = 0; i < dt.Rows[0]["imgUrl"].ToString().Split(':').Length - 1; i++) { %>
							<li>
								<a href="active_detail.aspx?id=<%= dt.Rows[0]["id"] %>"><img src="<%= dt.Rows[0]["imgUrl"].ToString().Split(':')[i] %>"></a>
							</li>
                            <% } %>
							
						</ul>
						<div class="slide_p">
							<h3><b>活动主题：<% = dt.Rows[0]["title"].ToString()%> </b></h3>
							<h3><b>活动介绍：</b></h3>
							<p><% = dt.Rows[0]["brief"].ToString()%></p>
							<h3><b>活动时间：<% = dt.Rows[0]["plan"].ToString()%></b></h3>
                            
                            <p>
								<a class="view_link" href="active_detail.aspx?id=<%= dt.Rows[0]["id"].ToString() %>">查看详情</a>
							</p>
                            
						</div>
					</div>
				</div>
			</div>

			<div class="content">
				<div class="wd1000 clearfix">
					<img src="img/ac_tit.png">
					<div class="hos_list clearfix">
						<img src="<%= dt.Rows[1]["imgUrl"].ToString().Split(':')[3] %>">
						<div class="right_txt">
							<h3>活动主题</h3>
							<p>
								<% = dt.Rows[1]["title"].ToString()%>
							</p>
							<h3>活动介绍</h3>
							<p>
								<% = dt.Rows[1]["brief"].ToString()%>
							</p>
							<h3>活动时间</h3>
							<p>
								<% = dt.Rows[1]["plan"].ToString()%><a class="view_link" href="active_detail.aspx?id=<%= dt.Rows[1]["id"].ToString() %>">查看详情</a>
							</p>
						</div>
					</div>
					<div class="hos_list clearfix">
						<img src="<%= dt.Rows[2]["imgUrl"].ToString().Split(':')[3] %>">
						<div class="right_txt">
							<h3>活动主题</h3>
							<p>
								<% = dt.Rows[2]["title"].ToString()%>
							</p>
							<h3>活动介绍</h3>
							<p>
								<% = dt.Rows[2]["brief"].ToString()%>
							</p>
							<h3>活动时间</h3>
							<p>
								<% = dt.Rows[2]["plan"].ToString()%>
								<!-- <br>
								CVS：2015年3月01日-2015年3月15日 --><a class="view_link" href="active_detail.aspx?id=<%= dt.Rows[2]["id"].ToString() %>">查看详情</a>
							</p>
						</div>
					</div>

				</div>
			</div>

			<div class="footer hei99">
				<div class="content">
					<p class="foot_txt">
						Copyright 2012 KAGOME(SHANGHAI) FOODS CO.,Ltd.All Rights Reserved.    沪ICP备06025276号
					</p>
				</div>
			</div>
		</div>
		<script>
		    $(function () {

		        slideActive();
		        function slideActive() {
		            var sWidth = $("#slide_bd").width();
		            //获取焦点图的宽度（显示面积）
		            var len = $("#slide_bd ul li").length;
		            //获取焦点图个数
		            var index = 0;
		            var picTimer;

		            //以下代码添加数字按钮和按钮后的半透明条，还有上一页、下一页两个按钮
		            var btn = "<div class='btn'>";
		            for (var i = 0; i < len; i++) {
		                btn += "<span></span>";
		            }
		            btn += "</div>";
		            //btn += "</div><div class='preNext pre'></div><div class='preNext next'></div>";
		            $("#slide_bd").append(btn);

		            //为小按钮添加鼠标滑入事件，以显示相应的内容
		            $("#slide_bd .btn span").css("opacity", 1).mouseenter(function () {
		                index = $("#slide_bd .btn span").index(this);
		                showPics(index);
		            }).eq(0).trigger("mouseenter");

		            //上一页、下一页按钮透明度处理
		            /*
					$(".ac_slide .preNext").css("opacity",0.2).hover(function() {
					$(this).stop(true,false).animate({"opacity":"1"},300);
					},function() {
					$(this).stop(true,false).animate({"opacity":"0.8"},300);
					});
					*/

		            //上一页按钮
		            $(".ac_slide .pre").click(function () {
		                index -= 1;
		                if (index == -1) {
		                    index = len - 1;
		                }
		                showPics(index);
		            });

		            //下一页按钮
		            $(".ac_slide .next").click(function () {
		                index += 1;
		                if (index == len) {
		                    index = 0;
		                }
		                showPics(index);
		            });

		            //本例为左右滚动，即所有li元素都是在同一排向左浮动，所以这里需要计算出外围ul元素的宽度
		            $("#slide_bd ul").css("width", sWidth * (len));
		            //鼠标滑上焦点图时停止自动播放，滑出时开始自动播放
		            $("#slide_bd").hover(function () {
		                clearInterval(picTimer);
		            }, function () {
		                picTimer = setInterval(function () {
		                    showPics(index);
		                    index++;
		                    if (index == len) {
		                        index = 0;
		                    }
		                }, 4000);
		                //此4000代表自动播放的间隔，单位：毫秒
		            }).trigger("mouseleave");

		            //显示图片函数，根据接收的index值显示相应的内容
		            function showPics(index) {//普通切换
		                var nowLeft = -index * sWidth;
		                //根据index值计算ul元素的left值
		                $("#slide_bd ul").stop(true, false).animate({
		                    "left": nowLeft
		                }, 300);
		                //通过animate()调整ul元素滚动到计算出的position
		                //$("#focus .btn span").removeClass("on").eq(index).addClass("on"); //为当前的按钮切换到选中的效果
		                $("#slide_bd .btn span").stop(true, false).removeClass("on").eq(index).stop(true, false).addClass("on");
		                //为当前的按钮切换到选中的效果
		            }

		        }

		    });

		</script>

	</body>
</html>


