﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="story_txt.aspx.cs" Inherits="Home_story_txt" %>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>可果美-商品信息</title>
		<link href="css/reset.css" rel="stylesheet" type="text/css">
		<link href="css/css.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.8.3.min.js"></script>
		<script src="js/com.js"></script>

        <script src="../js/jquery.qrcode-0.11.0.min.js"></script>
	</head>
	<body>
		<div class="index_bd">
			<!--导航-->
			<div class="sub_header">
				<h1 class="logo"><em><img src="img/logo_fiexd.png" alt="可果美"></em></h1>
				<ul class="nav_ul">
					<li>
						<a href="index.aspx">首页</a><span class="arr_link"></span>
					</li>
					<li class="cur">
						<a href="story_txt.aspx">品牌介绍</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a">
								<a href="story_txt.aspx#pplj">品牌理念</a> | <a href="story_txt.aspx#zzgy">制作工艺</a> | <a href="story_txt.aspx#dsj">可果美大事记</a>
							</div>
						</div>
					</li>
					<li>
						<a href="product.aspx">商品信息</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-indent:200px;">
								<a href="product.aspx#pro_1">纯正系列</a> | <a href="product.aspx#pro_2">果蔬生活系列</a> | <a href="product.aspx#pro_3">果蔬汁的价值</a>
							</div>
						</div>

					</li>
					<li>
						<a href="active_list.aspx">宣传活动</a><span class="arr_link"></span>
					</li>
					<li>
						<a href="shanghai.aspx">可果美上海</a><span class="arr_link"></span>
						<div class="sub_nav">
							<div class="nav_a" style="text-align:right;">
								<a href="shanghai.aspx#qyfz">企业概要及方针</a> | <a href="shanghai.aspx#new_list">可果美新闻</a> | <a href="shanghai.aspx#qa">Q&amp;A</a> | <a href="shanghai.aspx#lxfs">联系方式</a> | <a href="shanghai.aspx#xsqd">销售渠道</a>
							</div>
						</div>
					</li>
				</ul>

				<p class="wei_bd">
					<a class="weixin" href="qrcode.aspx" title="微信"></a><a class="weibo" href="qrcode.aspx" title="微博"></a>
				</p>
			</div>

			<div class="content">
				<!-- <img class="s_lpos" src="img/s/l_pso.png"> -->
				<img class="s_rpos" src="img/s/s_lrpos.png">
				<img class="s_lpos2" src="img/s/s_lrpos.png">
				<!--品牌理念-->
				<div class="ppln_bd wd1000 clearfix">
					<a name="pplj"></a>
					<img class="tit_pic" src="img/s/tit_1.png">
					<img class="ppln_pic" src="img/s/ppln_l.png">
					<div class="ppln_txt">
						<h3>素饮自然之美</h3>
						<h2><img src="img/s/ppln_tit2.png"></h2>
						<p>
							<b>蔬菜富含纤维质及多种微量元素，</b>
							<br>
							<b>是维持人体健康的天然食材。</b>
							<br>
							可果美以100%不添加的天然优质蔬果汁为身体补充每日“素”食需求，让生活在污染、失衡、过度添加中的我们在天然美味中，焕新身体素质，享受纯粹的素质生活
						</p>
						<h2><img src="img/s/ppln_tit1.png"></h2>
						<p>
							可果美专注研究“100%蔬果饮”，80年来致力将蔬果转化成更美味、更便利、更高效易人体吸收的蔬果汁营养补充。如今，可果美以国际领先技术通过理想食材配比，创造出绝佳蔬果饮养方案。以一份坚持呈现“蔬果之美”的匠心精神，为每一个追求优质生活的你在每一次饮用中收获简单、高效、天然的蔬果营养补充以及来自可果美最天然的问候。
						</p>
					</div>
				</div>
			</div>

			<div class="zzgy_bg">
				<!--制作工艺-->
				<div class="zzgy_bd">
					<div class="zzgy_line secton1"></div>
					<div class="zzgy_line secton2"></div>
					<a name="zzgy"></a>

					<img class="jingtian" src="img/s/zzgy.png">
					<div class="jtian_1">
						<h4><img src="img/s/jt_6.png"></h4>
						<p>
							我们遵循天时、地利、人和的自然道理
							<br>
							以蔬菜自然生长的天然规律进行栽培
							<br>
							收获上天最好的馈赠
						</p>
					</div>
					<div class="jtian_2 jt_1">
						<img class="img" src="img/s/jt_01.png">
						<h5>【土地】源自土地的美味 </h5>
						<p>
							土地是可果美的第一工厂，因此我们会为食材选择最适宜生长的土地，并悉心培育和严格管控。
						</p>
					</div>
					<div class="jtian_2 jt_2">
						<img class="img" src="img/s/jt_02.png">
						<h5>【种子】1/7500的荣耀 </h5>
						<p>
							种子，是可果美最宝贵的天然资产。
							因此，我们研究了全世界3/4的番茄品种，并储存了7500种左右番茄种子，最终以最合适的种子，制成符合不同需求的番茄汁。
						</p>
					</div>
					<div class="jtian_2 jt_3">
						<img class="img" src="img/s/jt_03.png">
						<h5>【收获】饱含自然盛意的完熟食材</h5>
						<p>
							可果美的食材全部采用露天栽培，且一年只有一次成熟期。在收获环节，我们根据食材的大小、颜色、形状以及质地，手工采摘出那些最优质的完熟食材，以获取其最好的味道及养分。
						</p>
					</div>

					<img class="xiwu_pic" src="img/s/xiwu.png">
					<div class="jtian_1 xiwu_1">
						<h4><img src="img/s/xiwu_tit.png"></h4>
						<p>
							因为珍惜每一个生长完满的食材
							<br>
							我们不断提升工艺，以更好的释放、保存
							<br>
							最终真实呈现食材的每一分天赋美质
						</p>
					</div>
					<div class="jtian_2 xiwu xwu_1">
						<img class="img" src="img/s/xw_01.png">
						<h5>【工艺】并非只用机器实现的高品质</h5>
						<p>
							从食材筛选到加工榨汁，再到封装运输， 每一道环节，可果美都进行科学严谨的管控，并通过专业榨汁技术，将食材营养最大化释放及保存。
						</p>
					</div>
					<div class="jtian_2 xiwu xwu_2">
						<img class="img" src="img/s/xw_02.png">
						<h5>【不添加】专研100%不添加的天然品质</h5>
						<p>
							为保证完美呈现蔬菜的天然品质，可果美坚持不添加防腐剂、色素、砂糖、食盐，结合完美的保鲜及杀菌技术，让每个人都能安心享用蔬果汁的天然美味。
						</p>
					</div>
					<div class="jtian_2 xiwu xwu_3">
						<img class="img" src="img/s/xw_03.png">
						<h5>【创新】不断创新，让蔬果汁再进化</h5>
						<p>
							从1933年品牌创立至今，可果美始终专注于蔬果汁领域，以高标准的专业精神，结合每个时代科技不断创新进化，从细节出发，为人们提供健康、纯正、无添加的自然之味。
						</p>
					</div>
				</div>
			</div>

			<!--大事记-->
			<div class="content">
				<a name="dsj"></a>

				<div class="dashiji_bd wd1000">
					<img class="dashiji_tit" src="img/s/dashi_tit.png">
					<div class="dashiji_top">
						<h2>可果美大事记</h2>
						<p>
							可果美专注于100%天然蔬菜汁/蔬果汁的研制，迄今已有一百多年历史, 
							<br>
							丰富的经验与不断尝新的态度，只为让天然的美味永恒流传。
						</p>
					</div>

					<div class="list_bd">
						<p class="arr_bg">
							<span class="t_bor"></span>
						</p>
						<div class="list">
							<div class="li_bd">
								<div class="l_jishi">
									<span class="pic"><img src="<%= dt.Rows[0]["imgUrl"] %>"></span>
									<h5><%= dt.Rows[0]["title"] %></h5>
									<p>
										<%= dt.Rows[0]["brief"] %>
									</p>
								</div>
							</div>
						</div>
						<div class="list">
							<div class="li_bd">
								<div class="r_jishi">
									<span class="pic"><img style="margin-top:-50px;" src="<%= dt.Rows[1]["imgUrl"] %>"></span>
									<h5><%= dt.Rows[1]["title"] %></h5>
									<p>
										<%= dt.Rows[1]["brief"] %>
									</p>
								</div>
							</div>
						</div>
						<div class="list">
							<div class="li_bd">
								<div class="l_jishi">
									<span class="pic"><img src="<%= dt.Rows[2]["imgUrl"] %>"></span>
									<h5><%= dt.Rows[2]["title"] %></h5>
									<p>
										<%= dt.Rows[2]["brief"] %>
									</p>
								</div>
							</div>
						</div>
						<div class="list">
							<div class="li_bd">
								<div class="r_jishi">
									<span class="pic"><img src="<%= dt.Rows[3]["imgUrl"] %>"></span>
									<h5><%= dt.Rows[3]["title"] %></h5>
									<p>
										<%= dt.Rows[3]["brief"] %>
									</p>
								</div>
							</div>
						</div>

						<div class="list">
							<div class="li_bd">
								<div class="l_jishi">
									<span class="pic"><img src="<%= dt.Rows[4]["imgUrl"] %>"></span>
									<h5><%= dt.Rows[4]["title"] %></h5>
									<p>
										<%= dt.Rows[4]["brief"] %>
									</p>
								</div>
							</div>
						</div>
						<div class="list">
							<div class="li_bd">
								<div class="r_jishi">
									<span class="pic"><img src="<%= dt.Rows[5]["imgUrl"] %>"></span>
									<h5><%= dt.Rows[5]["title"] %></h5>
									<p>
										<%= dt.Rows[5]["brief"] %>
									</p>
								</div>
							</div>
						</div>
						<div class="list">
							<div class="li_bd">
								<div class="r_jishi last">
									<h5><%= dt.Rows[6]["title"] %></h5>
									<p>
										<%= dt.Rows[6]["brief"] %>
									</p>
								</div>
							</div>
						</div>
						<p class="arr_bg">
							<span class="b_bor"></span>
						</p>
						<div class="list"></div>
						<div class="bg_jishi"></div>
					</div>
				</div>

			</div>

			<div class="footer hei99">
				<div class="content">
					<p class="foot_txt">
						Copyright 2012 KAGOME(SHANGHAI) FOODS CO.,Ltd.All Rights Reserved.    沪ICP备06025276号
					</p>
				</div>
			</div>
		</div>

	</body>
</html>
