﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="active_detail.aspx.cs" Inherits="Home_active_detail" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>可果美-产品详情</title>
<link href="css/reset.css" rel="stylesheet" type="text/css">
<link href="css/css.css" rel="stylesheet" type="text/css">
<script src="js/jquery-1.8.3.min.js"></script>
<script src="js/com.js"></script>

</head>
<body>
<div class="index_bd">
	<!--导航-->
	<div class="sub_header">
    	<h1 class="logo"><em><img src="img/logo_fiexd.png" alt="可果美"></em></h1>
        <ul class="nav_ul">
        	<li><a href="index.aspx">首页</a><span class="arr_link"></span></li>
        	<li>
            	<a href="story_txt.aspx">品牌介绍</a><span class="arr_link"></span>
                <div class="sub_nav">
                    <div class="nav_a">
                    <a href="story_txt.aspx#pplj">品牌理念</a> | <a href="story_txt.aspx#zzgy">制作工艺</a> | <a href="story_txt.aspx#dsj">可果美大事记</a>
                    </div>
                </div>            
            </li>
        	<li>
            	<a href="product.aspx">商品信息</a><span class="arr_link"></span>
                <div class="sub_nav">
                    <div class="nav_a" style="text-indent:200px;">
                    <a href="product.aspx#pro_1">纯正系列</a> | <a href="product.aspx#pro_2">果蔬生活系列</a> | <a href="product.aspx#pro_3">果蔬汁的价值</a>
                    </div>
                </div>
            
            </li>
        	<li class="cur"><a href="active_list.aspx">宣传活动</a><span class="arr_link"></span></li>
        	<li>
            	<a href="shanghai.aspx">可果美上海</a><span class="arr_link"></span>
                <div class="sub_nav">
                    <div class="nav_a" style="text-align:right;"> 
                    <a href="shanghai.aspx#qyfz">企业概要及方针</a> | <a href="shanghai.aspx#new_list">可果美新闻</a> | <a href="shanghai.aspx#qa">Q&amp;A</a> | <a href="shanghai.aspx#lxfs">联系方式</a> | <a href="shanghai.aspx#xsqd">销售渠道</a>
                    </div>
                </div>            
            </li>
        </ul>
        
        
        <p class="wei_bd"><a class="weixin" href="#" title="微信"></a><a class="weibo" href="#" title="微博"></a></p>        
    </div>
    
    <div class="content">
    	<div class="news_detail wd1000 clearfix">  
        	<div class="clearfix">     	
        	<div class="left_pic"><img src="<%= row["imgUrl"].ToString().Split(':')[3] %>"></div>
        	<div class="right_txt">
                <h1 class="h1_tit"><%= row["title"] %></h1>            
                <div class="share_bd"><span class="span">分享到：</span><a href="#"><img class="share_img" src="img/share_weixin.jpg"></a><a href="#"><img class="share_img" src="img/share_weibo.jpg"></a></div>
                <div class="time_address">
                    <p><b>时间：</b><%= row["plan"] %></p>
                    <p><b>地点：</b> 上海 浦东新区 浦东新舞台 浦东大道143号</p>
                </div>
                <h5 class="h5"><b>活动概要：</b></h5>
                <p class="news_p"><%= row["brief"] %></p>
            </div>
            </div>
            
            <div class="ac_detail">
                <p>
                 <%= row["content"] %>
                </p>
                 <h5 class="h5" style="padding-top: 20px;" ><b>活动照片：</b></h5>
                 <div class="slide_ad">
                 	<div class="big_img"><img src="img/temp/ac_imgbig.jpg"></div>
                 	<div class="small_list">
                    	<span class="to_left"><img src="img/right_arr.jpg"></span>
                        <div class="list_ul">
                            <ul>
                                <li><img src="img/temp/ac_imgs.jpg"></li>
                                <li><img src="img/temp/ac_img3.jpg"></li>
                                <li><img src="img/banner_active0.jpg"></li>
                                <li><img src="img/temp/ac_imgs.jpg"></li>
                                <li><img src="img/sh_img1.jpg"></li>
                                <li><img src="img/temp/ac_imgs.jpg"></li>
                                <li><img src="img/temp/ac_imgs.jpg"></li>
                                <li><img src="img/temp/ac_imgs.jpg"></li>
                                <li><img src="img/temp/ac_imgs.jpg"></li>
                                <li><img src="img/temp/ac_imgs.jpg"></li>
                                <li><img src="img/temp/ac_imgs.jpg"></li>
                                <li><img src="img/temp/ac_imgs.jpg"></li>
                                <li><img src="img/temp/ac_imgs.jpg"></li>
                            </ul>
                        </div>
                    	<span class="to_right"><img src="img/left_arr.jpg"></span>
                    </div>
                    
                 </div>                  
            </div>
        </div>
    </div>
    
    
    <div class="footer hei99">
    	<div class="content">        	
            <p class="foot_txt">Copyright 2012 KAGOME(SHANGHAI) FOODS CO.,Ltd.All Rights Reserved.    沪ICP备06025276号</p>
        </div>        
    </div>
</div>
<script>
    $(function () {
        $(".list_ul ul").width(($(".list_ul li").width() + 5) * $(".list_ul li").length);

        var sWidth = 920;//($(".list_ul li").width()+5)*$(".list_ul li").length; //获取焦点图的宽度（显示面积）
        var len = $(".list_ul li").length % 5; //获取焦点图个数
        var index = 0;
        var picTimer;


        //上一页按钮
        $(".to_left").click(function () {
            index -= 1;
            if (index == -1) { index = len - 1; }
            showPics(index);
        });

        //下一页按钮
        $(".to_right").click(function () {
            index += 1;
            if (index == len) { index = 0; }
            showPics(index);
        });



        function showPics(index) { //普通切换
            var nowLeft = -index * sWidth; //根据index值计算ul元素的left值
            $(".list_ul ul").stop(true, false).animate({ "left": nowLeft }, 300); //通过animate()调整ul元素滚动到计算出的position
            //$("#focus .btn span").removeClass("on").eq(index).addClass("on"); //为当前的按钮切换到选中的效果
            //$("#slide_bd .btn span").stop(true,false).removeClass("on").eq(index).stop(true,false).addClass("on"); //为当前的按钮切换到选中的效果
        }



        $(".list_ul li").click(function () {
            $(".big_img").html($(this).html());


        });




    })



</script>



</body>
</html>

