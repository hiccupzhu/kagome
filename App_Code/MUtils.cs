﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MUtils
/// </summary>
public class MUtils
{
	public MUtils()
	{
	}

    public static int CalcPerpageSize(int pageindex, int pagecout, int pagesize, int recodecount)
    {
        int per_size = 0;
        if (pagecout > 1)
        {
            per_size = (pageindex == pagecout - 1) ? (recodecount % pagesize) : pagesize;
        }
        if (per_size == 0) per_size = pagesize;


        return per_size;
    }
}