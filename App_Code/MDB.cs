﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data;
using System.Collections.Specialized;

/// <summary>
/// Summary description for Class1
/// </summary>
/// 

class MConn
{
    protected string m_connstr = "";
    protected OleDbConnection m_conn = null;

    public OleDbConnection conn
    {
        get { return m_conn; }
        set { m_conn = value;}
    }

    public MConn()
	{
        m_connstr = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        conn = new OleDbConnection(m_connstr);

        m_conn.Open();
	}

    ~MConn()
    {
        //if (conn != null)
        //{
        //    conn.Close();
        //}
    }

    public void close()
    {
        conn.Close();
    }
}

public class MDB
{
    

    public static object ExecuteScalar(string sql)
    {
        MConn cn = new MConn();

        OleDbCommand com = new OleDbCommand(sql, cn.conn);

        return com.ExecuteScalar();
    }

    public static void ExecuteNonQuery(string sql)
    {
        MConn cn = new MConn();

        OleDbCommand com = new OleDbCommand(sql, cn.conn);

        com.ExecuteNonQuery();

        cn.close();
    }

    public static DataTable QueryTable(string sql)
    {
        MConn cn = new MConn();

        DataTable dt = new DataTable();
        OleDbDataAdapter DA = new OleDbDataAdapter(sql, cn.conn);
        DA.Fill(dt);

        cn.close();
        return dt;
    }


    public static DataSet QueryDataSet(string sql)
    {
        MConn cn = new MConn();

        OleDbCommand com = new OleDbCommand(sql, cn.conn);

        OleDbDataAdapter adp = new OleDbDataAdapter(com);

        DataSet ds = new DataSet();

        adp.Fill(ds);

        cn.close();
        return ds;
    }


    //连贯操作
    public static int Count(string tname)
    {
        return Convert.ToInt32(ReadScalar(tname, "count(*)"));
    }

    public static void Create(string tname, NameValueCollection nvc)
    {
        string sql = "INSERT into [" + tname + "] ";

        string keys = " (";
        string values = " values(";

        foreach (string key in nvc)
        {
            keys += key + ",";
            values += "'" + nvc.Get(key) + "',";
        }

        keys = keys.Substring(0, keys.Length - 1);
        values = values.Substring(0, values.Length - 1);

        keys += ") ";
        values += ") ";

        sql += keys + values;

        ExecuteNonQuery(sql);
    }

    public static object Read(string tname, string fieds, string where = "")
    {
        return null;
    }

    public static DataTable ReadTable(string tname, string fieds, string where = "", string order = "")
    {
        string sql = "select " + fieds + " from [" + tname + "] ";
        if (where != "") sql += " where " + where;
        if (order != "") sql += " ORDER BY " + order;

        DataTable dt = QueryTable(sql);

        return dt;
    }

    public static object ReadScalar(string tname, string fieds, string where = "")
    {
        string sql = "select " + fieds + " from [" + tname + "] ";
        if (where != "") sql += " where " + where;

        return ExecuteScalar(sql);
    }

    

    public static void Updata(string tname, NameValueCollection nvc, string where)
    {
        string sql = "UPDATE [" + tname + "] SET ";

        foreach (string key in nvc)
        {
            sql += key + "='" + nvc.Get(key) + "',"; 
        }

        sql = sql.Substring(0, sql.Length - 1);

        sql += " where " + where;

        ExecuteNonQuery(sql);
    }



    public static void Delete(string tname, string where)
    {
        string sql = "DELETE FROM [" + tname + "] where " + where;

        ExecuteNonQuery(sql);
    }
}
