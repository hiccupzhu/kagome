﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MCookie
/// </summary>
public class MCookie
{
    public HttpResponse mResponse = null;
    public HttpRequest mRequest = null;
    public const string COOKIE_NAME = "KGMCookie";

	public MCookie(HttpResponse _response, HttpRequest _request)
	{
        mRequest = _request;
        mResponse = _response;
	}


    public void AddCookie(string name, string pass, string id)
    {
        HttpCookie cookie = new HttpCookie(COOKIE_NAME);//初使化并设置Cookie的名称
        DateTime dt = DateTime.Now;
        TimeSpan ts = new TimeSpan(0, 0, 30, 0, 0);//过期时间为1分钟
        cookie.Expires = dt.Add(ts);//设置过期时间
        cookie.Values.Add("userid", id.ToString());
        cookie.Values.Add("username", name);
        cookie.Values.Add("code", DateTime.Now.Ticks.ToString());
        mResponse.AppendCookie(cookie);

    }


    public string GetUserName()
    {
        HttpCookie cookie = mRequest.Cookies[COOKIE_NAME];

        if (cookie != null)
        {
            return cookie.Values["username"];
        }
        else
        {
            return null;
        }
    }

    public int GetUserID()
    {
        HttpCookie cookie = mRequest.Cookies[COOKIE_NAME];

        if (cookie != null)
        {
            return Convert.ToInt32(cookie.Values["userid"]);
        }
        else
        {
            return -1;
        }
    }

    public void RemoveCookie()
    {
        HttpCookie cookie = mRequest.Cookies[COOKIE_NAME];

        if (cookie == null) return;

        TimeSpan ts = new TimeSpan(-1, 0, 0, 0);
        cookie.Expires = DateTime.Now.Add(ts);
        cookie.Values["username"] = null;

        mResponse.AppendCookie(cookie);
    }


    public void Logout()
    {
        RemoveCookie();
    }
}