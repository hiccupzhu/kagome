﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Specialized;
using System.IO;

public partial class Admin_ContentEditProductClassification : System.Web.UI.Page
{
    protected DataRow row;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData2View();
        }
    }

    public void BindData2View()
    {
        DataTable dt = MDB.ReadTable("kgm_product_classification", "*", "id=" + Request["id"]);
        if (dt.Rows.Count > 0)
        {
            row = dt.Rows[0];

            title.Text = row["title"].ToString();
            brief.Text = row["brief"].ToString();

            ImageMain.ImageUrl = row["imgMain"].ToString();
            ImageTitle.ImageUrl = row["imgTitle"].ToString();
        }
    }

    protected void Commit_Click(object sender, EventArgs e)
    {
        NameValueCollection nvc = new NameValueCollection();

        nvc["title"] = title.Text;
        nvc["brief"] = brief.Text;
        nvc["imgMain"] = ImageMain.ImageUrl;
        nvc["imgTitle"] = ImageTitle.ImageUrl;

        MDB.Updata("kgm_product_classification", nvc, "id=" + Request["id"]);

        Page.ClientScript.RegisterStartupScript(GetType(), "message", "<script>alert('保存成功');</script>");
        Response.Redirect("ManagerProductClassification.aspx");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManagerProductClassification.aspx");
    }

    public string UploadImgSvr(FileUpload ful, Image img, string field, int idx = -1)
    {

        if (img.ImageUrl != "")
        {
            File.Delete(System.Web.HttpContext.Current.Server.MapPath(img.ImageUrl));
            img.ImageUrl = "";
        }


        UploadImg uimg = new UploadImg("/images/upload/classification");
        string ret = uimg.UpLoading(ful);

        if (ret != null && ret != "")
        {
            if (idx == -1)
            {
                NameValueCollection nvc = new NameValueCollection();
                nvc[field] = ret;

                MDB.Updata("kgm_product_classification", nvc, "id=" + Request["id"]);
            }
            

            img.ImageUrl = ret;
        }

        return "msg:OK";
    }

    protected void BTNTitle_Click(object sender, EventArgs e)
    {
        UploadImgSvr(FileUploadTitle, ImageTitle, "imgTitle");
    }

    protected void BTNMain_Click(object sender, EventArgs e)
    {
        UploadImgSvr(FileUploadMain, ImageMain, "imgMain");
    }
}