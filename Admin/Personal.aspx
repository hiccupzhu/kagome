﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Personal.aspx.cs" Inherits="Admin_Personal" MasterPageFile="~/Admin/MasterPage.master" Title="个人信息" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init(); // initlayout and core plugins

            $("#side_menu_personal").addClass("start active");

            $("#side_menu_personal_0").addClass("active");

        });

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
<div class="showBox">
    <form id="form1" runat="server">
    <div style="width:660px; height500px; margin:0 auto;">
        <div style="padding-top:200px; font-family:仿宋; font-size:20px;" >
            <label style="font-size:20px; font-weight:bold;">用户名：</label>
            <asp:Label ID="txtUsername" Font-Names="仿宋" Font-Size="20px" runat="server"></asp:Label>
            <br />
            <label style="font-size:20px; font-weight:bold;">邮 箱：</label>
            <asp:Label ID="txtEmail" Font-Names="仿宋" Font-Size="20px" runat="server"></asp:Label>
        </div>
    </div>
    </form>
</div>
</asp:Content>
