﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Admin_ContentEditActivity : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //FileUpload1.Attributes.Add("onchange", "uploadImage();");

        
    }

    public string UploadImgSvr(FileUpload ful, Image img, int idx)
    {
        
        if (img.ImageUrl != "")
        {
            File.Delete(System.Web.HttpContext.Current.Server.MapPath(img.ImageUrl));
            img.ImageUrl = "";
        }
        

        UploadImg uimg = new UploadImg("/images/upload/activity");
        string ret = uimg.UpLoading(ful);

        if (ret != null && ret != "")
        {
            UpdataDB(ret, idx);

            img.ImageUrl = ret;
        }
        
        return "msg:OK";
    }

    public void UpdataDB(string url, int idx)
    {
        string[] imgs = MDB.ExecuteScalar("select imgUrl from [kgm_activity] where id=" + Request["id"]).ToString().Split(':');

        if (imgs.Length == 1)
        {
            imgs = new string[4];
        }

        imgs[idx - 1] = url;

        string imgUrl = "";
        for(int i = 0; i < imgs.Length; i++){
            imgUrl += imgs[i] + ":";
        }
        imgUrl = imgUrl.Substring(0, imgUrl.Length - 1);

        MDB.ExecuteNonQuery("UPDATE [kgm_activity] set imgUrl='" + imgUrl + "' where id=" + Request["id"]);

    }

    protected void BTN1_Click(object sender, EventArgs e)
    {
        UploadImgSvr(FileUpload1, IMAGE1, 1);
        
    }

    protected void BTN2_Click(object sender, EventArgs e)
    {
        UploadImgSvr(FileUpload2, IMAGE2, 2);
    }

    protected void BTN3_Click(object sender, EventArgs e)
    {
        UploadImgSvr(FileUpload3, IMAGE3, 3);
    }

    protected void BTN4_Click(object sender, EventArgs e)
    {
        UploadImgSvr(FileUpload4, IMAGE4, 4);
    }
}