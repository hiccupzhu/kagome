﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContentEditProduct.aspx.cs" Inherits="Admin_ContentEditProduct"
    MasterPageFile="~/Admin/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript" src="http://img.baidu.com/hunter/ueditor.js"></script>


    <script type="text/javascript">
        window.UEDITOR_HOME_URL = "ueditor/";

        var editorOption = {
            initialFrameWidth: 750,
            initialFrameHeight: 600
        };
        var ue = new baidu.editor.ui.Editor(editorOption);
        //ue.render("ContentPlaceHolder1_editor");
        ue.render("ContentPlaceHolder1_editor");

        $('form').submit(function () {
            $('#content').val(ue.getContent());
        });


        jQuery(document).ready(function () {
            App.init(); // initlayout and core plugins
            $("#side_menu_content").addClass("start active");

            ue.ready(function () {
                //ue.setContent($("#ContentPlaceHolder1_editor").val());
            });
            
        });

        function prePage() {
            window.location.href = "ManagerProduct.aspx";
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
<div class="showBox" style="padding-left:5px; height:1470px;">
    <form method="post" runat="server">
        <div id="ImagesBox" style="width:30%;height:100%; float:left;overflow:hidden;" class="minddle">

            <div class="appmsg">
                <div style="font-size:16px; background-color: #c0c0c0; width:80%; height:25px;" class="minddle" >产品详细图</div>
                <div class="appmsg_thumb minddle">
                    <asp:Image ID="IMAGE1" runat="server" />
                    <asp:FileUpload ID="FileUpload1" CssClass="appmsg_upload" runat="server" />
                </div>
                <asp:LinkButton ID="BTN1" CssClass="btn" OnClick="BTN1_Click" runat="server"><span>上传图片</span></asp:LinkButton>
            </div>
            
        </div>



        <div style="width:70%;height:100%;overflow:auto; float:left">

            <div style="margin-top:50px; margin-left:50px;">
                <div style="width:100%;">
                    <label>产品名称：</label> <asp:TextBox ID="txtName" name="name" Width="80%" runat="server"></asp:TextBox>
                </div>
                <div style="width:50%; float:left">
                    
                    <label>生产许可证编号：</label> <asp:TextBox ID="txtLicenseNumber" runat="server"></asp:TextBox>
                    <label>配料表：</label> <asp:TextBox ID="txtPeiliaobiao" runat="server"></asp:TextBox>
                    <label>保质期：</label> <asp:TextBox ID="txtBaozhiqi" runat="server"></asp:TextBox>
                    <label>包装方式：</label> <asp:TextBox ID="txtBaozhuang" runat="server"></asp:TextBox>
                    <label>分类：</label> <asp:DropDownList ID="txtClassification" runat="server"></asp:DropDownList>
                    <label>商品条形码：</label> <asp:TextBox ID="txtTiaoxingma" runat="server"></asp:TextBox>
                </div>

                <div style="width:50%; float:left">
                    <label>是否含糖：</label> <asp:DropDownList ID="txtShifoutang" runat="server"></asp:DropDownList>
                    <label>产地：</label> <asp:TextBox ID="txtChandi" runat="server"></asp:TextBox>
                    <label>规格：</label> <asp:TextBox ID="txtGuige" runat="server"></asp:TextBox>
                    <label>淘宝链接：</label> <asp:TextBox ID="txtTaobao" runat="server"></asp:TextBox>
                    <label>京东链接：</label> <asp:TextBox ID="txtJingdong" runat="server"></asp:TextBox>
                    <label>1号店链接：</label> <asp:TextBox ID="txtYihaodian" runat="server"></asp:TextBox>
                </div>

                <div style="width:100%; clear:both;">
                    <label>产品简介：</label> <asp:TextBox ID="txtBrief" Width="740px" Height="80px" TextMode="MultiLine" runat="server"></asp:TextBox>
                    <label>宣传内容：</label> <asp:TextBox ID="editor" Width="750px"  TextMode="MultiLine" runat="server"></asp:TextBox>
                </div>
                
            </div>

            

            <div style="clear:both; padding-top:20px; padding-left:50px;">
                <!--
                <button type="submit">提交</button>
                -->
                <asp:Button OnClick="Commit_Click" Width="80px" Text="提交" runat="server" />
                <asp:Button onclick="Back_Click"  Width="80px" Text="返回" runat="server"  />
                <asp:Label ID="txtID" Visible="false" runat="server"></asp:Label>
            </div>
        </div>
    </form>
</div>
</asp:Content>