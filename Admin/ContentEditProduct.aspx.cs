﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Security.Cryptography;
using System.Web.Security;
using System.Data;
using System.Collections.Specialized;

public partial class Admin_ContentEditProduct : System.Web.UI.Page
{
    

    protected void Page_Load(object sender, EventArgs e)
    {
        //Page.ClientScript.RegisterStartupScript(GetType(), "message", "<script>alert('Request.Form.Count=" + Request.Form.Count + "');</script>");

        if (!IsPostBack)
        {
            DataTable dt = MDB.QueryTable("select title from [kgm_product_classification]");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                txtClassification.Items.Add(dt.Rows[i][0].ToString());
            }



            txtShifoutang.Items.Add("含糖");
            txtShifoutang.Items.Add("不含糖");

            txtID.Text = Request["id"];

            if (txtID.Text == "") return;

            dt = MDB.QueryTable("select * from [kgm_product] where id=" + txtID.Text);


            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                txtName.Text = row["name"].ToString();
                txtLicenseNumber.Text = row["license_number"].ToString();
                txtLicenseNumber.Text = row["license_number"].ToString();
                txtPeiliaobiao.Text = row["peiliaobiao"].ToString();
                txtBaozhiqi.Text = row["baozhiqi"].ToString();
                txtBaozhuang.Text = row["baozhuang"].ToString();
                txtClassification.Text = row["classification"].ToString();
                txtTiaoxingma.Text = row["tiaoxingma"].ToString();
                txtShifoutang.Text = row["shifoutang"].ToString();
                txtChandi.Text = row["chandi"].ToString();
                txtGuige.Text = row["guige"].ToString();
                txtTaobao.Text = row["taobao"].ToString();
                txtJingdong.Text = row["jingdong"].ToString();
                txtYihaodian.Text = row["yihaodian"].ToString();
                txtBrief.Text = row["brief"].ToString();
                editor.Text = row["content"].ToString();

                IMAGE1.ImageUrl = row["imgUrl"].ToString();

            }
        }

    }


    protected void Commit_Click(object sender, EventArgs e)
    {
        NameValueCollection nvc = new NameValueCollection();

        nvc["name"] = txtName.Text;
        
        nvc["license_number"] = txtLicenseNumber.Text;
        nvc["peiliaobiao"] = txtPeiliaobiao.Text;
        nvc["baozhiqi"] = txtBaozhiqi.Text;
        nvc["baozhuang"] = txtBaozhuang.Text;
        nvc["classification"] = txtClassification.Text;
        nvc["tiaoxingma"] = txtTiaoxingma.Text;
        nvc["shifoutang"] = txtShifoutang.Text;
        nvc["chandi"] = txtChandi.Text;
        nvc["guige"] = txtGuige.Text;
        nvc["taobao"] = txtTaobao.Text;
        nvc["jingdong"] = txtJingdong.Text;
        nvc["yihaodian"] = txtYihaodian.Text;
        nvc["brief"] = txtBrief.Text;
        nvc["content"] = editor.Text;

        //Page.ClientScript.RegisterStartupScript(GetType(), "message", "<script>alert('editor.Text=" + editor.Text + "');</script>");
        MDB.Updata("kgm_product", nvc, "id=" + txtID.Text);
        

        Page.ClientScript.RegisterStartupScript(GetType(), "message", "<script>alert('保存成功');</script>");
        Response.Redirect("ManagerProduct.aspx");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManagerProduct.aspx");
    }


    public string UploadImgSvr(FileUpload ful, Image img, string field, int idx = -1)
    {

        if (img.ImageUrl != "")
        {
            File.Delete(System.Web.HttpContext.Current.Server.MapPath(img.ImageUrl));
            img.ImageUrl = "";
        }


        UploadImg uimg = new UploadImg("/images/upload/product");
        string ret = uimg.UpLoading(ful);

        if (ret != null && ret != "")
        {
            if (idx == -1)
            {
                NameValueCollection nvc = new NameValueCollection();
                nvc[field] = ret;

                MDB.Updata("kgm_product", nvc, "id=" + Request["id"]);
            }
            else
            {
                NameValueCollection nvc = new NameValueCollection();
                nvc[field] = GetImageUrlProduct(ret, field, idx);

                MDB.Updata("kgm_product", nvc, "id=" + Request["id"]);
            }

            img.ImageUrl = ret;
        }

        return "msg:OK";
    }

    public string GetImageUrlProduct(string url, string field, int idx)
    {
        string[] imgs = MDB.ReadScalar("kgm_product", field, "id=" + Request["id"]).ToString().Split(':');

        if (imgs.Length == 1)
        {
            imgs = new string[4];
        }

        imgs[idx - 1] = url;

        string imgUrl = "";
        for (int i = 0; i < imgs.Length; i++)
        {
            imgUrl += imgs[i] + ":";
        }
        imgUrl = imgUrl.Substring(0, imgUrl.Length - 1);


        return imgUrl;

    }

    protected void BTN1_Click(object sender, EventArgs e)
    {
        UploadImgSvr(FileUpload1, IMAGE1, "imgUrl");
    }

 
}