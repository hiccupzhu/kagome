﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_Personal : System.Web.UI.Page
{
    public MCookie mCookie = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (mCookie == null) mCookie = new MCookie(Response, Request);

        int userid = mCookie.GetUserID();
        if (userid < 0)
        {
            mCookie.Logout();
            Response.Redirect("Login.aspx");
        }

        txtUsername.Text = mCookie.GetUserName();

        string sqlSel = @"select [email] from [kgm_user] where id=" + userid ;
        DataTable dt = MDB.QueryTable(sqlSel);
        string email = dt.Rows[0][0].ToString();


        txtEmail.Text = email;
    }
}