﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContentEditProductClassification.aspx.cs" Inherits="Admin_ContentEditProductClassification"
    MasterPageFile="~/Admin/MasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript" src="http://img.baidu.com/hunter/ueditor.js"></script>


    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init(); // initlayout and core plugins
            $("#side_menu_content").addClass("start active");

        });

        function prePage() {
            window.location.href = "ManagerProductClassification.aspx";
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
<div class="showBox" style="padding-left:5px;">
    <form method="post" runat="server">
        <div id="ImagesBox" style="width:30%;height:100%; float:left;overflow:hidden;" class="minddle">
            <div class="appmsg">
                <div style="font-size:16px; background-color: #c0c0c0; width:80%; height:25px;" class="minddle" >产品宣传头图</div>
                <div class="appmsg_thumb minddle">
                    <asp:Image ID="ImageTitle" runat="server" />
                    <asp:FileUpload ID="FileUploadTitle" CssClass="appmsg_upload" runat="server" />
                </div>
                <asp:LinkButton ID="BTNTitle" CssClass="btn" OnClick="BTNTitle_Click" runat="server"><span>上传图片</span></asp:LinkButton>
            </div>


            <div class="appmsg">
                <div style="font-size:16px; background-color: #c0c0c0; width:80%; height:25px;" class="minddle" >产品宣传图</div>
                <div class="appmsg_thumb minddle">
                    <asp:Image ID="ImageMain" runat="server" />
                    <asp:FileUpload ID="FileUploadMain" CssClass="appmsg_upload" runat="server" />
                </div>
                <asp:LinkButton ID="BTNMain" CssClass="btn" OnClick="BTNMain_Click" runat="server"><span>上传图片</span></asp:LinkButton>
            </div>

        </div>



        <div style="width:70%;height:100%;overflow:auto; float:left">

            <div style="margin-top:50px; margin-left:50px;">
                <div style="width:100%;">
                    <label>分类名称：</label> <asp:TextBox ID="title" name="name" Width="740px" runat="server"></asp:TextBox>
                </div>

                <div style="width:100%; clear:both;">
                    <label>分类简介：</label> <asp:TextBox ID="brief" Width="740px" Height="400px" TextMode="MultiLine" runat="server"></asp:TextBox>
                </div>
                
            </div>

            

            <div style="clear:both; padding-top:20px; padding-left:50px;">
                <!--
                <button type="submit">提交</button>
                -->
                <asp:Button OnClick="Commit_Click" Width="80px" Text="提交" runat="server" />
                <asp:Button onclick="Back_Click"  Width="80px" Text="返回" runat="server"  />
                <asp:Label ID="txtID" Visible="false" runat="server"></asp:Label>
            </div>
        </div>
    </form>
</div>
</asp:Content>