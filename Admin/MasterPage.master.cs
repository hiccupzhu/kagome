﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_MasterPage : System.Web.UI.MasterPage
{
    public MCookie mCookie = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (mCookie == null) mCookie = new MCookie(Response, Request);

        string action = Request.QueryString["action"];
        if (action == "logout")
        {
            mCookie.Logout();
            Response.Redirect("Login.aspx");
        }

        string username = mCookie.GetUserName();
        if (username == null) Response.Redirect("Login.aspx");


        txtUserName.Text = username;
    }
}
