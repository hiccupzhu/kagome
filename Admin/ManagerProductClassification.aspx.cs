﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Specialized;

public partial class Admin_ManagerProductClassification : System.Web.UI.Page
{
    private MCookie mCookie = null;
    private int mPageSize = 4;
    private int mPageIndex;
    private int mPageCount;
    private int mRecordCount;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (mCookie == null) mCookie = new MCookie(Response, Request);
        if (mCookie.GetUserName() == null) Response.Redirect("Login.aspx");

        mRecordCount = MDB.Count("kgm_product_classification");
        mPageCount = (mRecordCount + (mPageSize - 1)) / mPageSize;
        mPageIndex = Math.Max(0, Convert.ToInt32(txtPageIndex.Text) - 1);



        if (!IsPostBack)
        {
            BindGridView();
        }

        
    }


    //SELECT * FROM (
    //    SELECT TOP [%d] * FROM (
    //        SELECT TOP [%d * (%d + 1)] * FROM [kgm_product_classification] ORDER BY id DESC
    //    ) ORDER BY id ASC
    //)ORDER BY id DESC
    private void BindGridView()
    {
        int per_size = MUtils.CalcPerpageSize(mPageIndex, mPageCount, mPageSize, mRecordCount);

        string sql =
        "SELECT * FROM (" +
            "SELECT TOP " + per_size + " * FROM ( " +
                "SELECT TOP " + (mPageSize * (mPageIndex + 1)) + " * FROM [kgm_product_classification] ORDER BY id DESC" +
            " ) ORDER BY id ASC" +
        ")ORDER BY id DESC";


        DataSet ds = MDB.QueryDataSet(sql);
        ;
        this.GridView1.DataSource = ds.Tables[0];
        this.GridView1.DataBind();


        txtPageIndex.Text = (mPageIndex + 1).ToString();
        txtPageCount.Text = mPageCount.ToString();
    }


    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGridView();
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        BindGridView();
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        string id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();

        NameValueCollection nvc = new NameValueCollection();

        nvc["title"] = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("TextBox1")).Text;
        nvc["brief"] = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("TextBox2")).Text;

        MDB.Updata("kgm_product_classification", nvc, "id=" + id);

 

        GridView1.EditIndex = -1;
        BindGridView();

    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string strDel = "DELETE FROM [kgm_product_classification] where id=" + id;

        MDB.ExecuteNonQuery(strDel);

        BindGridView();

    }


    protected void Add_Click(object sender, EventArgs e)
    {
        string strInsert = "insert into [kgm_product_classification] (title) values('')";

        MDB.ExecuteNonQuery(strInsert);

        BindGridView();
    }

    protected void BtnFirstPage_Click(object sender, EventArgs e)
    {
        mPageIndex = 0;
        BindGridView();
    }

    protected void BtnNextPage_Click(object sender, EventArgs e)
    {
        mPageIndex = Math.Min(mPageIndex + 1, mPageCount - 1);

        BindGridView();
    }

    protected void BtnPrePage_Click(object sender, EventArgs e)
    {
        mPageIndex = Math.Max(0, mPageIndex - 1);

        BindGridView();
    }

    protected void BtnTailPage_Click(object sender, EventArgs e)
    {
        mPageIndex = mPageCount - 1;
        BindGridView();
    }
}