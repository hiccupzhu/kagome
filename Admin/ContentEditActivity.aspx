﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContentEditActivity.aspx.cs"
     Inherits="Admin_ContentEditActivity" Title="编辑页面" MasterPageFile="~/Admin/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript" src="http://img.baidu.com/hunter/ueditor.js"></script>


    <script type="text/javascript">
        window.UEDITOR_HOME_URL = "ueditor/";

        var ue = UE.getEditor("editor");

        var request = new Object();

        

        //ueditor must ready,you can use setContent;
        //Or:ueditor  Cannot set property 'innerHTML' of undefined
        ue.addListener("ready", function () {
            $.get("ContentEditHandler.ashx" + window.location.search,
                function (data) {
                    var infos = data.split("<||>");

                    console.info(infos);

                    $("#id").val(infos[0]);
                    $("#title").val(infos[1]);
                    $("#brief").val(infos[2]);
                    ue.setContent(infos[3]);
                    $("#plan").val(infos[4]);
                    $("#addr").val(infos[5]);

                    
                    var imgs = infos[6].split(":");
                    console.info("    >> imgUrl:" + infos[6] + "            imgs.length=" + imgs.length);
                    for (var i = 0; i < 4; i++) {
                        var src = typeof( imgs[i]) == "undefined" ? "" : imgs[i];
                        console.info("          src[" + i + "]=" + src);
                        $("#ContentPlaceHolder1_IMAGE" + (i+1)).attr("src", src)
                    }
            });
        });
 
        jQuery(document).ready(function () {
            App.init(); // initlayout and core plugins
            $("#side_menu_content").addClass("start active");

            request = RequestUrl2Array(window.location.search);

        });


        function commitContent() {

            var content = ue.getContent();
            var id = $("#id").val();
            var brief = $("#brief").val();
            var title = $("#title").val();
            var plan = $("#plan").val();
            var addr = $("#addr").val();
            var imgUrl = "";
         
            
            for (var i = 1; i <= 4; i++) {
                imgUrl += $("#ContentPlaceHolder1_IMAGE" + i).attr("src") + ":";
            }
            

            imgUrl = imgUrl.substr(0, imgUrl.length - 1);
            

            console.info("imgUrl:" + imgUrl);
            

            request["action"] = "autoset";
            request["tn"] = "kgm_activity";
            request["id"] = id;

            
            $.post("ContentEditHandler.ashx?" + Array2RequestUrl(request),
               {
                   "id": id,
                   "title": title,
                   "brief": brief,
                   "imgUrl": imgUrl,
                   "content": content,
                   "plan": plan,
                   "addr": addr
               },
               function (data) {
                   alert(data);
                   window.location.href = document.referrer;
               });
               
        }

        function resetContent() {
            UE.getEditor("editor").setContent("");
        }

        function prePage() {
            window.location.href = document.referrer;
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
<div class="showBox" style="padding-left:5px; height:970px;">
    
    <form runat="server">
        
        <div id="ImagesBox" style="width:35%;height:100%; float:left;overflow:auto;" class="minddle">
                <div class="appmsg">
                    <div class="appmsg_thumb minddle">
                        <asp:Image ID="IMAGE1" runat="server" />
                        <asp:FileUpload ID="FileUpload1" CssClass="appmsg_upload" runat="server" />
                    </div>
                    <asp:LinkButton ID="BTN1" CssClass="btn" OnClick="BTN1_Click" runat="server"><span>上传图片</span></asp:LinkButton>
                </div>


                <div class="appmsg">
                    <div class="appmsg_thumb minddle">
                        <asp:Image ID="IMAGE2" runat="server" />
                        <asp:FileUpload ID="FileUpload2" CssClass="appmsg_upload" runat="server" />
                    </div>
                    <asp:LinkButton ID="BTN2" CssClass="btn" OnClick="BTN2_Click" runat="server"><span>上传图片</span></asp:LinkButton>
                </div>


                <div class="appmsg">
                    <div class="appmsg_thumb minddle">
                        <asp:Image ID="IMAGE3" runat="server" />
                        <asp:FileUpload ID="FileUpload3" CssClass="appmsg_upload" runat="server" />
                    </div>
                    <asp:LinkButton ID="BTN3" CssClass="btn" OnClick="BTN3_Click" runat="server"><span>上传图片</span></asp:LinkButton>
                </div>

            
                <div class="appmsg middle">
                    <div style="font-size:16px; background-color: #c0c0c0; width:70%; height:25px;" class="minddle" >副图</div>
                    <div class="appmsg_thumb minddle">
                        <asp:Image ID="IMAGE4" runat="server" />
                        <asp:FileUpload ID="FileUpload4" CssClass="appmsg_upload" runat="server" />
                    </div>
                    <asp:LinkButton CssClass="btn" ID="BTN4" OnClick="BTN4_Click" runat="server"><span>上传图片</span></asp:LinkButton>
                </div>
            
        </div>

    </form>

    <!--
        注意这里没用form是因为：用post请求后，ueditor其中以组件又会以get的方式提交一次；原因不明
    -->
    <div style="width:65%;height:100%; float:left;overflow:auto;">
        <label>主题</label>
        <input name="title" type="text" id="title" style="overflow:auto; width:50%;" />
        <input name="id" type="text" id="id" style="visibility:hidden; width:0px; margin:0; padding:0;" />
        
        <label>简介</label>
        <textarea name="brief" id="brief" style="overflow:auto; width:80%; height:80px;"></textarea>

        <!--
            注意这里没有用asp:Textbox标签是因为在母版系统中，asp:TextBox标签不能正常渲染ueditor。
        -->
        <label>正文</label>
        <textarea name="content" id="editor" type="text/plain" style="width:98%;height:40%;"></textarea>

        <label>安排</label>
        <textarea name="plan" id="plan" style="overflow:auto; width:80%;"></textarea>

        <label>地点</label>
        <textarea name="addr" id="addr" style="overflow:auto; width:80%;"></textarea>

        <br />
        <!--
        <button onclick="commitContent()" style="width:80px;" >提交</button>
        -->
        <button type="button" onclick="commitContent()" style="width:80px;">提交</button>
        <button type="button" onclick="resetContent()"  style="width:80px;" >重置</button>
        <button type="button" onclick="prePage()"  style="width:80px;" >返回</button>
    </div>
    
</div>
</asp:Content>