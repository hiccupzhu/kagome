﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Collections.Specialized;

public partial class Admin_ContentEditNews : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string UploadImgSvr(FileUpload ful, Image img, int idx)
    {

        if (img.ImageUrl != "")
        {
            File.Delete(System.Web.HttpContext.Current.Server.MapPath(img.ImageUrl));
            img.ImageUrl = "";
        }


        UploadImg uimg = new UploadImg("/images/upload/news");
        string ret = uimg.UpLoading(ful);

        if (ret != null && ret != "")
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["imgUrl"] = ret;

            MDB.Updata(Request["tn"], nvc , "id=" + Request["id"]);
            img.ImageUrl = ret;
        }

        return "msg:OK";
    }

    protected void BTN1_Click(object sender, EventArgs e)
    {
        UploadImgSvr(FileUpload1, IMAGE1, 0);
    }
}