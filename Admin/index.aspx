﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Admin_Default" MasterPageFile="~/Admin/MasterPage.master" Title="后台管理主页"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        jQuery(document).ready(function () {

            App.init(); // initlayout and core plugins

            $("#side_menu_home").addClass("start active");
        });

        
    </script>
</asp:Content>





<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 

<!-- BEGIN PAGE -->
<div class="showBox">

        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <div id="portlet-config" class="modal hide">

            <div class="modal-header">

                <button data-dismiss="modal" class="close" type="button"></button>

                <h3>Widget Settings</h3>

            </div>

            <div class="modal-body">

                Widget settings form goes here

            </div>

        </div>

        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- BEGIN PAGE CONTAINER-->

        <div class="container-fluid">

            <!-- BEGIN PAGE HEADER-->

            <div class="row-fluid">

                <div class="span12">

                    <!-- BEGIN STYLE CUSTOMIZER -->

                    <div class="color-panel hidden-phone">

                        <div class="color-mode-icons icon-color"></div>

                        <div class="color-mode-icons icon-color-close"></div>

                        <div class="color-mode">

                            <p>THEME COLOR</p>

                            <ul class="inline">

                                <li class="color-black current color-default" data-style="default"></li>

                                <li class="color-blue" data-style="blue"></li>

                                <li class="color-brown" data-style="brown"></li>

                                <li class="color-purple" data-style="purple"></li>

                                <li class="color-grey" data-style="grey"></li>

                                <li class="color-white color-light" data-style="light"></li>

                            </ul>

                            <label>

                                <span>Layout</span>

                                <select class="layout-option m-wrap small">

                                    <option value="fluid" selected>Fluid</option>

                                    <option value="boxed">Boxed</option>

                                </select>

                            </label>

                            <label>

                                <span>Header</span>

                                <select class="header-option m-wrap small">

                                    <option value="fixed" selected>Fixed</option>

                                    <option value="default">Default</option>

                                </select>

                            </label>

                            <label>

                                <span>Sidebar</span>

                                <select class="sidebar-option m-wrap small">

                                    <option value="fixed">Fixed</option>

                                    <option value="default" selected>Default</option>

                                </select>

                            </label>

                            <label>

                                <span>Footer</span>

                                <select class="footer-option m-wrap small">

                                    <option value="fixed">Fixed</option>

                                    <option value="default" selected>Default</option>

                                </select>

                            </label>

                        </div>

                    </div>

                    <!-- END BEGIN STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->

                    <h3 class="page-title">

                        仪表盘 <small>statistics and more</small>

                    </h3>

                    <ul class="breadcrumb">

                        <li>

                            <i class="icon-home"></i>

                            <a href="index.aspx">Home</a>

                            <i class="icon-angle-right"></i>

                        </li>

                        <li><a href="#">Dashboard</a></li>

                        <li class="pull-right no-text-shadow">

                            <div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">

                                <i class="icon-calendar"></i>

                                <span></span>

                                <i class="icon-angle-down"></i>

                            </div>

                        </li>

                    </ul>

                    <!-- END PAGE TITLE & BREADCRUMB-->

                </div>

            </div>

            <!-- END PAGE HEADER-->

            <div id="dashboard">

                <!-- BEGIN DASHBOARD STATS -->

                <div class="row-fluid">
                    <div class="span3 responsive" data-tablet="span6" data-desktop="span3">

                        <div class="dashboard-stat purple">

                            <div class="visual">

                                <i class="icon-bar-chart"></i>

                            </div>

                            <div class="details">

                                <div class="number">1235</div>

                                <div class="desc">今日访客</div>

                            </div>

                            <a class="more" href="#">

                                View more <i class="m-icon-swapright m-icon-white"></i>

                            </a>

                        </div>

                    </div>


                    <div class="span3 responsive" data-tablet="span6" data-desktop="span3">

                        <div class="dashboard-stat blue">

                            <div class="visual">

                                <i class="icon-comments"></i>

                            </div>

                            <div class="details">

                                <div class="number"> 849 </div>

                                <div class="desc"> 最新留言 </div>

                            </div>

                            <a class="more" href="#">

                                View more <i class="m-icon-swapright m-icon-white"></i>

                            </a>

                        </div>

                    </div>

                </div>

                <!-- END DASHBOARD STATS -->




            </div>

        </div>

        <!-- END PAGE CONTAINER-->

    </div>
<!-- END PAGE -->
   
</asp:Content>
