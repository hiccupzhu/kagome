﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContentEditNews.aspx.cs"
     Inherits="Admin_ContentEditNews" Title="编辑页面" MasterPageFile="~/Admin/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
    <!--    <script type="text/javascript" src="http://img.baidu.com/hunter/ueditor.js"></script>    <script type="text/javascript" src="../ueditor"></script>    -->

    <script type="text/javascript">
        window.UEDITOR_HOME_URL = "ueditor/";
        var ue = UE.getEditor("editor");

        var request = new Object();
        
        //ueditor must ready,you can use setContent;
        //Or:ueditor  Cannot set property 'innerHTML' of undefined
        ue.addListener("ready", function () {
            $.get("ContentEditHandler.ashx" + window.location.search,
                function (data) {
                    var infos = data.split("<||>");

                    console.info(infos);

                    $("#txtID").val(infos[0]);
                    $("#ContentPlaceHolder1_IMAGE1").attr("src", infos[1]);
                    
                    $("#txtTitle").val(infos[2]);
                    $("#txtBreif").val(infos[3]);
                    ue.setContent(infos[4]);
            });
        });
 
        jQuery(document).ready(function () {
            App.init(); // initlayout and core plugins
            $("#side_menu_content").addClass("start active");

            request = RequestUrl2Array(window.location.search);
        });


        function commitContent() {
            var content = ue.getContent();
            var id = $("#txtID").val();
            var brief = $("#txtBreif").val();
            var title = $("#txtTitle").val();
            var imgUrl = $("#ContentPlaceHolder1_IMAGE1").attr("src");
            
            //var data = title + "<||>" + brief + "<||>" + content;
            //console.info(content);

            request["action"] = "autoset";
            request["id"] = id;


            $.post("ContentEditHandler.ashx?" + Array2RequestUrl(request),
               {
                   "id": id,
                   "title": title,
                   "brief": brief,
                   "imgUrl": imgUrl,
                   "content": content
               },
               function (data) {
                   alert(data);
                   window.location.href = document.referrer;
               });
        }

        function resetContent() {
            UE.getEditor("editor").setContent("");
        }

        function prePage() {
            window.location.href = document.referrer;
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
<div class="showBox" style="padding-left:5px; height:770px;">
    <form runat="server">
        
        <div id="ImagesBox" style="width:35%;height:100%; float:left;overflow:auto;" class="minddle">
            <div class="appmsg">
                <div class="appmsg_thumb minddle">
                    <asp:Image ID="IMAGE1" runat="server" />
                    <asp:FileUpload ID="FileUpload1" CssClass="appmsg_upload" runat="server" />
                </div>
                <asp:LinkButton ID="BTN1" CssClass="btn" OnClick="BTN1_Click" runat="server"><span>上传图片</span></asp:LinkButton>
            </div>
        </div>

    </form>

    <div style="width:65%;height:100%;overflow:auto;">
        <label>标题</label>
        <input type="text" id="txtTitle" style="overflow:auto; width:50%;" />
        <input type="text" id="txtID" style="visibility:hidden; width:0px; margin:0; padding:0;" />
        
        
        <label>摘要</label>
        <textarea  id="txtBreif" style="overflow:auto; width:80%;"></textarea>

        <!--
            主页这里没有使用asp:textbox是因为，ueditor在母版状态下无法正常刷新（单页面正常），
            所以现在选用原生态的js来获取和提交数据
        -->
        <label>正文</label>
        <script type="text/plain"  id="editor" style="width:98%;height:50%;"></script>

        
        <br />
        <button onclick="commitContent()" style="width:80px;" >提交</button>
        <button onclick="resetContent()"  style="width:80px;" >重置</button>
        <button onclick="prePage()"  style="width:80px;" >返回</button>
    </div>
</div>
</asp:Content>