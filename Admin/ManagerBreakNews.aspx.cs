﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_ManagerBreakNews : System.Web.UI.Page
{
    private MCookie mCookie = null;
    private int mPageSize = 4;
    private int mPageIndex;
    private int mPageCount;
    private int mRecordCount;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (mCookie == null) mCookie = new MCookie(Response, Request);
        if (mCookie.GetUserName() == null) Response.Redirect("Login.aspx");


        mRecordCount = MDB.Count("kgm_breaknews");
        mPageCount = (mRecordCount + (mPageSize - 1)) / mPageSize;
        mPageIndex = Math.Max(0, Convert.ToInt32(txtPageIndex.Text) - 1);



        if (!IsPostBack)
        {
            BindGridView();
        }
    }


    //SELECT * FROM (
    //    SELECT TOP [%d] * FROM (
    //        SELECT TOP [%d * (%d + 1)] * FROM [kgm_breaknews] ORDER BY id DESC
    //    ) ORDER BY id ASC
    //)ORDER BY id DESC
    private void BindGridView()
    {
        int per_size = MUtils.CalcPerpageSize(mPageIndex, mPageCount, mPageSize, mRecordCount);


        string sql =
        "SELECT * FROM (" +
            "SELECT TOP " + (per_size) + " * FROM ( " +
                "SELECT TOP " + (mPageSize * (mPageIndex + 1)) + " * FROM [kgm_breaknews] ORDER BY id DESC" +
            " ) ORDER BY id ASC" +
        ")ORDER BY id DESC";

        DataSet ds = MDB.QueryDataSet(sql);

        this.GridView1.DataSource = ds.Tables[0];
        this.GridView1.DataBind();


        txtPageIndex.Text = (mPageIndex + 1).ToString();
        txtPageCount.Text = mPageCount.ToString();
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string strDel = "DELETE FROM [kgm_breaknews] where id=" + id;

        MDB.ExecuteNonQuery(strDel);

        BindGridView();

    }


    protected void Add_Click(object sender, EventArgs e)
    {
        int user_id = mCookie.GetUserID();
        string strInsert = "insert into [kgm_breaknews] (brief) values('')";

        MDB.ExecuteNonQuery(strInsert);

        string sql = "select id from [kgm_breaknews] ORDER BY id DESC";
        string id = Convert.ToString(MDB.ExecuteScalar(sql));


        BindGridView();

        Response.Redirect("ContentEditNews.aspx?action=get&tn=kgm_breaknews&id=" + id + "&fields=id,title,brief,content");
    }


    protected void BtnFirstPage_Click(object sender, EventArgs e)
    {
        mPageIndex = 0;
        BindGridView();
    }

    protected void BtnNextPage_Click(object sender, EventArgs e)
    {
        mPageIndex = Math.Min(mPageIndex + 1, mPageCount - 1);

        BindGridView();
    }

    protected void BtnPrePage_Click(object sender, EventArgs e)
    {
        mPageIndex = Math.Max(0, mPageIndex - 1);

        BindGridView();
    }

    protected void BtnTailPage_Click(object sender, EventArgs e)
    {
        mPageIndex = mPageCount - 1;
        BindGridView();
    }
}