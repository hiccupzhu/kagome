﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.OleDb;


public partial class Admin_Login : System.Web.UI.Page
{
    public MCookie mCookie = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (mCookie == null) mCookie = new MCookie(Response, Request);
        

        if (mCookie.GetUserName() != null)
        {
            Response.Redirect("index.aspx");
        }

        if (Request.RequestType.ToUpper() == "POST" && !IsPostBack)
        {
            if (LoginProc(Request["username"], Request["password"]))
            {
                Response.Redirect("index.aspx");
            }
        }
        
    }

    public void LoginBtn_Click(object sender, EventArgs e)
    {
        //string name = txtUserName.Text;
        //string pass = txtUserpass.Text;
    }

    public bool LoginProc(string name, string pass)
    {
        if (name == null || name == "" || pass == null || pass == "") return false;

        bool ret = false;
        //获取验证码
        string code = Request["checkcode"];
        //判断用户输入的验证码是否正确
        if (Request.Cookies["CheckCode"].Value == code)
        {
            if (CheckLogin(name, pass))
            {
                string sqlSel = @"select id from [kgm_user] where username='" + name + "' and password='" + pass + "'";
                int userid = Convert.ToInt32(MDB.ExecuteScalar(sqlSel));

                mCookie.AddCookie(name, pass, userid.ToString());
                //ClientScript.RegisterStartupScript(Page.GetType(), "", "<script>alert('登录成功！')</script>");
                msgShowBox.Text = "登录成功！";

                ret = true;
            }
            else
            {
                //RegisterStartupScript("", "<script>alert('用户名或密码错误！')</script>");
                //ClientScript.RegisterStartupScript(Page.GetType(), "", "<script>alert('用户名或密码错误！')</script>");
                msgShowBox.Text = "用户名或密码错误！";

                

                ret = false;
            }
        }
        else
        {
            //RegisterStartupScript("", "<script>alert('验证码输入错误！')</script>");
            //Response.Write("<script>alert('验证码输入错误！');</script>");
            //ClientScript.RegisterStartupScript(Page.GetType(), "", "<script>alert('验证码输入错误！')</script>");
            msgShowBox.Text = "验证码输入错误！";
            ret = false;
        }

        return ret;
    }


    public bool CheckLogin(string name, string password)
    {
        bool ret = false;

        string sqlSel = @"select count(*) from [kgm_user] where username='" + name + "' and password='" + password + "'";

        if (Convert.ToInt32(MDB.ExecuteScalar(sqlSel)) > 0)
        {
            ret = true;
        }
        else
        {
            ret = false;
        }

        return ret;
    }


    
}