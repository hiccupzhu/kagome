﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ManagerProduct.aspx.cs" Inherits="Admin_ManagerProduct" MasterPageFile="~/Admin/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init(); // initlayout and core plugins

            $("#side_menu_content").addClass("start active");

            $("#sub_menu_content_pr").addClass("active");

        });

    </script>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
<div class="showBox">
    <form id="form1" runat="server">
    <div class="portlet-body PaddingBox">
        <div class="clearfix">            
            <div class="btn-group">
                <asp:LinkButton ID="IDAddBtn" class="btn green" OnClick="Add_Click" runat="server">
                    <i class="icon-plus"></i> 添加新行

                </asp:LinkButton>
            </div>
        </div>
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                CssClass="GridViewStyle"
                AlternatingRowStyle-CssClass="AltRowStyle"
                HeaderStyle-CssClass="HeaderStyle"
                SelectedRowStyle-CssClass="SelectedRowStyle"
                RowStyle-CssClass="RowStyle"

                OnRowDeleting="GridView1_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="编号" ItemStyle-Width="50px" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="name" HeaderText="名称" ItemStyle-Width="180px" InsertVisible="False" ReadOnly="True" SortExpression="ID" />

                    <asp:BoundField DataField="peiliaobiao" HeaderText="配料包" ItemStyle-Width="130px" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="baozhiqi" HeaderText="保质期" ItemStyle-Width="80px" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="baozhuang" HeaderText="包装" ItemStyle-Width="80px" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="classification" HeaderText="产品分类" ItemStyle-Width="120px" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="shifoutang" HeaderText="是否含糖" ItemStyle-Width="120px" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="guige" HeaderText="规格" ItemStyle-Width="150px" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="chandi" HeaderText="产地" ItemStyle-Width="150px" InsertVisible="False" ReadOnly="True" SortExpression="ID" />

                    <asp:TemplateField HeaderText="操作"  ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">  
                        <ItemTemplate>  
                            <a href="ContentEditProduct.aspx?action=get&tn=kgm_product&id=<%# Eval("id") %>"><img src="../images/buttons/edit1.png" />  </a>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/buttons/del.png" CommandName="Delete" />  
                        </ItemTemplate>  
                        <EditItemTemplate>  
                            <asp:LinkButton ID="Button1" runat="server" Text="更新" CommandName="Update" />  
                            <asp:LinkButton ID="Button3" runat="server" Text="取消" CommandName="Cancel" />  
                        </EditItemTemplate>  
                    </asp:TemplateField> 
                </Columns>
            
            </asp:GridView>
        </div>

        <table style="margin-left:auto; margin-right:auto; margin-top:20px;">
            <tr>
                <td><asp:LinkButton CssClass="btn" ID="BtnFirstPage" OnClick="BtnFirstPage_Click" Text="首&nbsp;&nbsp;页" runat="server" /></td>
                <td><asp:LinkButton CssClass="btn" ID="BtnPrePage" OnClick="BtnPrePage_Click" Text="上一页" runat="server" /></td>
                <td><asp:Label  ID="txtPageIndex" Text="0" Font-Underline="true" runat="server" />/<asp:Label  ID="txtPageCount" Text="0" Font-Underline="true" runat="server" /></td>
                <td><asp:LinkButton CssClass="btn" ID="BtnNextPage" OnClick="BtnNextPage_Click" Text="下一页" runat="server" /></td>
                <td><asp:LinkButton CssClass="btn" ID="BtnTailPage" OnClick="BtnTailPage_Click" Text="尾&nbsp;&nbsp;页" runat="server" /></td>
            </tr>
        </table>
     </div>

    </form>
</div>

                
</asp:Content>
