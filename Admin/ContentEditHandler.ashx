﻿<%@ WebHandler Language="C#" Class="ContentEditHandler" %>

using System;
using System.Web;
using System.Data;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

public class ContentEditHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";

        //URL:?action=get&tn=kgm_news&id=0&fields=title,brief,content
        switch (context.Request["action"])
        {
            case "get":
                {
                    string tname = context.Request["tn"];
                    string id = context.Request["id"];
                    string fields = context.Request["fields"];
                    context.Response.Write(GetContentFromDB(tname, id, fields));
                }
                break;
            case "set":
                {
                    string tname = context.Request["tn"];
                    string id = context.Request["id"];
                    string fields = context.Request["fields"];
                    string data = context.Request["data"];

                    context.Response.Write(SetContentFromDB(tname, id, fields, data));
                }
                break;
            case "autoset":
                {
                    string tname = context.Request["tn"];
                    context.Response.Write(UpdateTable(tname, context.Request.Form));
                }
                break;
            case "insert":
                {
                    
                }
                break;
            default:
                break;
        }
    }

    public string UpdateTable(string tname, NameValueCollection nvc)
    {
        string content = nvc["id"];

        string sql = "UPDATE [" + tname + "] SET ";
        
        foreach (string key in nvc)
        {
            if (key == "id" || 
                key == "tn" || 
                key == "action" || 
                key == "fields" ||
                key == "" ||
                key == null    
                ) continue;
            
            sql += key + "='" + nvc.Get(key) + "',"; 
        }

        sql = sql.Substring(0, sql.Length - 1);

        sql += " where id=" + nvc["id"];


        MDB.ExecuteNonQuery(sql);

        return "保存成功";
    }


    public string GetContentFromDB(string tname, string _id, string _fields)
    {
        string sql = "select " + _fields + " from [" + tname + "] where id=" + _id;

        DataTable dt = MDB.QueryTable(sql);

        string strData = "";
        string [] fields = _fields.Split(',');

        for (int i = 0; i < fields.Length; i++)
        {        
            strData += dt.Rows[0][i].ToString();
            if (i != fields.Length - 1)
            {
                strData += "<||>";
            }
        }

        return strData;
    }
    
    public string SetContentFromDB(string tname, string _id, string _fields, string data)
    {
        string [] fields = _fields.Split(',');
        
        string[] ds = Regex.Split(data, @"<\|\|>", RegexOptions.IgnoreCase);

        string sql = "UPDATE [" + tname + "] SET ";
        for (int i = 0; i < fields.Length; i++)
        {
            sql += fields[i] + "='" + ds[i] + "'";
            if (i != fields.Length - 1)
            {
                sql += ",";
            }
        }
        sql += " where id=" + _id;


        MDB.ExecuteNonQuery(sql);

        return "保存成功";
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}