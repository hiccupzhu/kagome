﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContentEditIdxBanner.aspx.cs" Inherits="Admin_ManagerIdxBanner"
    MasterPageFile="~/Admin/MasterPage.master" Title="主页管理" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript" src="http://img.baidu.com/hunter/ueditor.js"></script>


    <script type="text/javascript">
        
        jQuery(document).ready(function () {
            App.init(); // initlayout and core plugins
            $("#side_menu_content").addClass("start active");

            $("#sub_menu_content_ib").addClass("active");

        });

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
<div class="showBox" style="padding-left:5px; height:1470px;">
    <form method="post" runat="server">
        <div id="ImagesBox" style="width:100%;height:100%;overflow:hidden;" class="minddle">

            <div class="appmsg">
                <div style="font-size:16px; background-color: #c0c0c0; width:80%; height:25px;" class="minddle" >首页主图</div>
                <div class="appmsg_thumb minddle" style="width:560px; height:270px;">
                    <asp:Image ID="IMAGE1" runat="server" />
                    <asp:FileUpload ID="FileUpload1" CssClass="appmsg_upload" runat="server" />
                </div>
                <asp:LinkButton ID="BTN1" CssClass="btn" OnClick="BTN1_Click" runat="server"><span>上传图片</span></asp:LinkButton>
            </div>
            

            <div style="clear:both; padding-top:20px; padding-left:50px;">
                <label>主页标题：</label> <asp:TextBox ID="title" name="name" Width="80%" runat="server"></asp:TextBox>
                <label>主页链接：</label> <asp:TextBox ID="hyperlink" name="name" Width="80%" runat="server"></asp:TextBox>

                <br />
                <asp:Button OnClick="Commit_Click" Width="80px" Text="提交" runat="server" />
                <asp:Button OnClick="Back_Click"  Width="80px" Text="返回" runat="server"  />
                <asp:TextBox ID="txtID" Visible="false" runat="server"></asp:TextBox>
            </div>
        </div>     
       
    </form>
</div>
</asp:Content>