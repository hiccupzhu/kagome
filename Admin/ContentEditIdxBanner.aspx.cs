﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Collections.Specialized;

public partial class Admin_ManagerIdxBanner : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int num = MDB.Count("kgm_index_banner");
            if (num < 1)
            {
                NameValueCollection nvc = new NameValueCollection();
                nvc["title"] = "";

                MDB.Create("kgm_index_banner", nvc);
            }
            BindData2View();
        }
    }

    protected void BindData2View()
    {
        DataTable dt = MDB.ReadTable("kgm_index_banner", "*");
        if (dt.Rows.Count < 1) return;
        DataRow r = dt.Rows[0];

        IMAGE1.ImageUrl = r["imgUrl"].ToString();
        title.Text = r["title"].ToString();
        hyperlink.Text = r["hyperlink"].ToString();
        txtID.Text = r["id"].ToString();
    }

    public string UploadImgSvr(FileUpload ful, Image img, string field, int idx = -1)
    {

        if (img.ImageUrl != "")
        {
            File.Delete(System.Web.HttpContext.Current.Server.MapPath(img.ImageUrl));
            img.ImageUrl = "";
        }


        UploadImg uimg = new UploadImg("/images/upload/index");
        string ret = uimg.UpLoading(ful);

        if (ret != null && ret != "")
        {
            if (idx == -1)
            {
                NameValueCollection nvc = new NameValueCollection();
                nvc[field] = ret;

                MDB.Updata("kgm_index_banner", nvc, "id=" + txtID.Text);
            }


            img.ImageUrl = ret;
        }

        return "msg:OK";
    }

    protected void BTN1_Click(object sender, EventArgs e)
    {
        UploadImgSvr(FileUpload1, IMAGE1, "imgUrl");
    }

    protected void Commit_Click(object sender, EventArgs e)
    {
        NameValueCollection nvc = new NameValueCollection();
        nvc["title"] = title.Text;
        nvc["hyperlink"] = hyperlink.Text;

        MDB.Updata("kgm_index_banner", nvc, "id=" + txtID.Text);

        Page.ClientScript.RegisterStartupScript(GetType(), "message", "<script>alert('保存成功');</script>");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManagerIdxBanner.aspx");
    }
}