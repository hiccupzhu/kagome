﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ManagerActivity.aspx.cs"
     Inherits="Admin_ManagerActivity" MasterPageFile="~/Admin/MasterPage.master" Title="活动管理" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init(); // initlayout and core plugins

            $("#side_menu_content").addClass("start active");

            $("#sub_menu_content_ac").addClass("active");
           
        });

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
<div class="showBox">
    <form id="form1" runat="server">
    <div class="portlet-body PaddingBox">
        <div class="clearfix">            
            <div class="btn-group">
                <asp:LinkButton ID="IDAddBtn" class="btn green" OnClick="Add_Click" runat="server">
                    <i class="icon-plus"></i> 添加新行

                </asp:LinkButton>
            </div>
        </div>
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                CssClass="GridViewStyle"
                AlternatingRowStyle-CssClass="AltRowStyle"
                HeaderStyle-CssClass="HeaderStyle"
                SelectedRowStyle-CssClass="SelectedRowStyle"
                RowStyle-CssClass="RowStyle"


                OnRowCancelingEdit="GridView1_RowCancelingEdit"   
                OnRowUpdating="GridView1_RowUpdating"  
                OnRowEditing="GridView1_RowEditing"   
                OnRowDeleting="GridView1_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="编号" ItemStyle-Width="40px" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="createddate" ItemStyle-Width="70px" HeaderText="创建日期" ReadOnly="True" SortExpression="createddate" />

                    <asp:TemplateField HeaderText="活动主题" ItemStyle-Width="200px">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("title") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" Text='<%# Bind("title") %>' style="width:100%; max-width:100%; height:100px; max-height:100px;" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="活动简介" ItemStyle-Width="560px">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("brief") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" Text='<%# Bind("brief") %>' style="width:100%; max-width:100%; height:100px; max-height:100px;" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="活动安排" ItemStyle-Width="200px">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("plan") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" Text='<%# Bind("plan") %>' style="width:100%; max-width:100%; height:100px; max-height:100px;" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="操作"  ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">  
                        <ItemTemplate>  
                            <!--<asp:ImageButton ID="Button2" runat="server" ImageUrl="~/images/buttons/edit1.png" CommandName="Edit" />-->
                            <a href="ContentEditActivity.aspx?action=get&tn=kgm_activity&id=<%# Eval("id") %>&fields=id,title,brief,content,plan,addr,imgUrl"><img src="../images/buttons/edit1.png" />  </a>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/buttons/del.png" CommandName="Delete" />  
                        </ItemTemplate>  
                        <EditItemTemplate>  
                            <asp:LinkButton ID="Button1" runat="server" Text="更新" CommandName="Update" />  
                            <asp:LinkButton ID="Button3" runat="server" Text="取消" CommandName="Cancel" />  
                        </EditItemTemplate>  
                    </asp:TemplateField>


                </Columns>
            </asp:GridView>
        </div>
       
        

        <table style="margin-left:auto; margin-right:auto; margin-top:20px;">
            <tr>
                <td><asp:LinkButton CssClass="btn" ID="BtnFirstPage" OnClick="BtnFirstPage_Click" Text="首&nbsp;&nbsp;页" runat="server" /></td>
                <td><asp:LinkButton CssClass="btn" ID="BtnPrePage" OnClick="BtnPrePage_Click" Text="上一页" runat="server" /></td>
                <td><asp:Label  ID="txtPageIndex" Text="0" Font-Underline="true" runat="server" />/<asp:Label  ID="txtPageCount" Text="0" Font-Underline="true" runat="server" /></td>
                <td><asp:LinkButton CssClass="btn" ID="BtnNextPage" OnClick="BtnNextPage_Click" Text="下一页" runat="server" /></td>
                <td><asp:LinkButton CssClass="btn" ID="BtnTailPage" OnClick="BtnTailPage_Click" Text="尾&nbsp;&nbsp;页" runat="server" /></td>
            </tr>
        </table>
    </div>
    </form>
</div>
</asp:Content>
